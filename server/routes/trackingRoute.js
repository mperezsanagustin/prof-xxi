const express = require("express");
const router = express.Router();
const cors = require("cors");
const auth = require("../middleware/auth");
const dbo = require("../database/dbmongo");

const {
    send,
  } = require("../controllers/tracking");

router.use(cors({ credentials: true, origin: "http://localhost:3000" }));

// router.post("/send", send);
router.route("/send").post(function (req, response) {
  let db_connect = dbo.getDb();
  let myobj = {
    actor: {
        objectType: "Agent",
        mbox: "mailto:Tester@example.com",
        name: "TESTER22"
    },
    verb: {
        id: "http://example.com/xapi/viewed",
        display: {
            "en-US": "viewed"
        }
    },
    object: {
        id: "http://example.com/video",
        definition: {
            name: {
                "en-US": "video"
            },
            description: {
                "en-US": "Example video"
            }
        },
        objectType: "Activity"
    }
  };
  db_connect.collection("tracking").insertOne(myobj, function (err, res) {
    if (err) throw err;
    response.json(res);
  });
 });

module.exports = router;
