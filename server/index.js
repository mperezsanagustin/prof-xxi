require("dotenv").config({ path: "./config.env" });
const express = require("express");
const cors = require("cors");
// const mongoose = require("mongoose");

// mongoose.connect("mongodb+srv://xAPI_statements:admin123@cluster0.wxey3rl.mongodb.net/project?retryWrites=true&w=majority");

const app = express();
app.use(express.json());

const userRoute = require("./routes/userRoute");
app.use(
  process.env.ENV_NODE === "production" ? "/api/user" : "/user",
  userRoute
);

const unitRoute = require("./routes/unitRoute");
app.use(
  process.env.ENV_NODE === "production" ? "/api/units" : "/units",
  unitRoute
);

const linkRoute = require("./routes/linkRoute");
app.use(
  process.env.ENV_NODE === "production" ? "/api/links" : "/links",
  linkRoute
);

const dashboardRoute = require("./routes/dashboardRoute");
app.use(
  process.env.ENV_NODE === "production" ? "/api/dashboards" : "/dashboards",
  dashboardRoute
);

const initiativeRoute = require("./routes/initiativeRoute");
app.use(
  process.env.ENV_NODE === "production" ? "/api/initiatives" : "/initiatives",
  initiativeRoute
);

//const trackingRoute = require("./routes/trackingRoute");
//app.use("/tracking", trackingRoute);

//const dbmongo = require("./database/dbmongo");

app.listen(3001, () => {
  // perform a database connection when server starts
  //dbmongo.connectToServer(function (err) {
  //  if (err) console.error(err);

  //});
  ///////

  console.log("runing server on port 3001");
});
