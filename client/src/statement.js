
var MongoClient = require('mongodb').MongoClient;
export function test(){
var url = "mongodb://localhost:27017/project";
// make client connect to mongo service
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    // db pointing to newdb
    console.log("Switched to "+db.databaseName+" database");
 
    // document to be inserted
    var doc = {
        id: "88e334d9-47c1-4ccb-bda9-c95519b38299",
        actor: {
            objectType: "Agent",
            mbox: "mailto:Tester@example.com",
            name: "TESTER"
        },
        verb: {
            id: "http://example.com/xapi/read",
            display: {
                "en-US": "read"
            }
        },
        object: {
            id: "http://example.com/text",
            definition: {
                name: {
                    "en-US": "benchmarking"
                },
                description: {
                    "en-US": "Example xAPI text"
                }
            },
            objectType: "Activity"
        }
    };
     
    // insert document to 'users' collection using insertOne
    db.collection("Tracking").insertOne(doc, function(err, res) {
        if (err) throw err;
        console.log("Document inserted");
        // close the connection to db when you are done with it
        db.close();
    });
});
}

// var TinCan = require('tincanjs');
// export function Statement(verbID,verb,targetID,targetName,targetDescription) {
//     var lrs = new TinCan.LRS(
//         {
//             endpoint: "https://cloud.scorm.com/lrs/SCDTNP5AS5/sandbox/",
//             username: "record.storage.xapi@gmail.com",
//             password: "Talent&irit9",
//             allowFail: false
//         }
//     )  

//     var statement = new TinCan.Statement(
//     {
//         actor: {  
//             mbox: "mailto:Tester@example.com",  
//             name: "TESTER",  
//             objectType: "Agent"  
//         },  
//         verb: {  
//             id: verbID,  
//             display: {"en-US": verb}  
//         },  
//         target: {  
//             id: targetID,  
//             definition: {  
//                 name: {"en-US": targetName},  
//                 description: {"en-US": targetDescription}  
//             },  
//             objectType: "Activity"  
//         }  
//     }
//     )

//     lrs.saveStatement(
//         statement,
//         {
//             callback: function (err, xhr) {
//                 if (err !== null) {
//                     if (xhr !== null) {
//                         console.log("Failed to save statement: " + xhr.responseText + " (" + xhr.status + ")");
//                         return;
//                     }

//                     console.log("Failed to save statement: " + err);
//                     return;
//                 }

//                 console.log("Statement saved");
//             }
//         }
//     )


// }