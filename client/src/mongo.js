import { MongoClient } from "mongodb";

export default async function run() {
    console.log('Calling run');
    const uri = "mongodb://localhost:27017";
    const client = new MongoClient(uri);
  try {
    const database = client.db("project");
    const Tracking = database.collection("Tracking");
    // create a document to insert
    const doc = {
      title: "Record of a Shriveled Datum",
      content: "No bytes, no problem. Just insert a document, in MongoDB",
      end: "verificationm",
    }
    const result = await Tracking.insertOne(doc);

    console.log(`A document was inserted with the _id: ${result.insertedId}`);
  } finally {
    await client.close();
  }
}
