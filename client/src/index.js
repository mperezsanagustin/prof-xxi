import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

import "./i18n";

ReactDOM.render(
  <React.Suspense fallback={<div>Loading</div>}>
    <App />
  </React.Suspense>,
  document.getElementById("root")
);
