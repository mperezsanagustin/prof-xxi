import React, { useState, useEffect } from "react";
import "./Dashboards.css";
import "./Popup";
import { Link } from "react-router-dom";
import axios from "axios";
import PieChart from "./charts/PieChart";
import BarChart from "./charts/BarChart";
import HorizontalChart from "./charts/HorizontalBarChart";
import Gauge from "./charts/Gauge";
import RadarChart from "./charts/RadarChart";
import LineChart from "./charts/LineChart";
import DistanceBarChart from "./charts/DistanceBarChart";
import { Timeline, ShowChart, MultilineChart } from "@material-ui/icons";
import ReactTooltip from "react-tooltip";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function UnitAnalytics() {
  const [unit, setUnit] = useState({
    unitId: "",
    unitName: "",
    unitType: "",
    unitDescription: "",
    accessType: "",
  });

  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );

  const [units, setUnits] = useState([]);
  const [text, setText] = useState("");
  const [loading, setLoading] = useState(true);

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${profile.token}`,
    },
  };

  const { t } = useTranslation();

  const [buttonPopup, setButtonPopup] = useState(false);
  const [buttonPopup1, setButtonPopup1] = useState(false);

  ////////////////////////////////////////////////////////////////////
  const fetchData = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/units/manageunit"
        : "http://localhost:3001/units/manageunit",
      config
    );
    setUnits(data.unitsResult);
  };

  const fetchCurrentUnit = () => {
    console.log("Entering fetch Current");
    units.map((element) => {
      if (`${element.idunit}` === unit.unitId) {
        setUnit({
          unitId: unit.unitId,
          unitName: element.name,
          unitType: element.type,
          unitDescription: element.description,
          accessType: element.access,
        });
      }
    });
  };

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUnit({ ...unit, [fieldName]: fieldValue });
  };
  ////////////////////////////////////////////////////////////////////////
  // Fetching answers of the current unit

  const [pie, setPie] = useState({});

  const fetchPie = async () => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/pie/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/pie/${unit.unitId}`,
      config
    );
    setPie(data.pieData);
  };
  ///////////////////////////////////////////////////////////////////////

  const [bar, setBar] = useState({});

  const fetchBar = async () => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/bar/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/bar/${unit.unitId}`,
      config
    );
    setBar(data.barData);
    console.log(data.barData);
  };
  ///////////////////////////////////////////////////////////////////////

  const [hbar, setHbar] = useState({});

  const fetchHbar = async () => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/hbar/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/hbar/${unit.unitId}`,
      config
    );
    setHbar(data.hbarData);
  };

  ////////////////////////////////////////////////////////////////////////

  const [gauge, setGauge] = useState(1);

  const fetchGauge = async () => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/gauge/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/gauge/${unit.unitId}`,
      config
    );
    setGauge(data.gaugeData);
  };

  ///////////////////////////////////////////////////////////////////////

  const [radar, setRadar] = useState({});

  const fetchRadar = async () => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/radar/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/radar/${unit.unitId}`,
      config
    );
    setRadar({ unitName: unit.unitName, data: data.radarData });
  };

  ////////////////////////////////////////////////////////////////

  const [distBar, setDistBar] = useState({});

  const fetchDistBar = async () => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/distbar/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/distbar/${unit.unitId}`,
      config
    );
    setDistBar({ unitName: unit.unitName, data: data.distBarData });
  };
  ///////////////////////////////////////////////////////////////////////

  const [dateIn, setDateIn] = useState({});
  const [dateOut, setDateOut] = useState({});
  const [line, setLine] = useState({});

  const fetchLine = async () => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/line/${unit.unitId}/${dateIn}/${dateOut}`
        : `http://localhost:3001/dashboards/myunitsanalysis/line/${unit.unitId}/${dateIn}/${dateOut}`,
      config
    );
    setLine(data.lineData);
    console.log(data.lineData);
  };

  ///////////////////////////////////////////////////////////////////////

  const renderUnitAnalytics = async () => {
    if (!(unit.unitId === "")) {
      setText("");
      await fetchPie();

      await fetchBar();

      await fetchHbar();

      await fetchGauge();

      await fetchRadar();

      await fetchDistBar();

      await fetchLine();
    } else {
      setPie({});
      setBar({});
      setGauge(1);
      setHbar({});
      setRadar({});
      setDistBar({});
      setLine({});
      setText("Please select a valid Unit");
    }
  };
  /////////////////////////////////////////////////////////////////////
  useEffect(() => {
    document.title = "My Units Analytics";
    setProfile(JSON.parse(localStorage.getItem("profile")));
    if (!profile?.token) {
      window.location = "/login";
    }

    fetchData();
  }, []);

  useEffect(() => {
    fetchCurrentUnit();
    renderUnitAnalytics();
  }, [unit.unitId, unit.unitName, dateIn, dateOut]);

  return (
    <div>
      <div className="container">
        <div className="charts-container">
          <ul className="sidebarList">
            <li className="sidebarListItem">
              <Link to="/dashboards/unitanalytics">
                <button>
                  <ShowChart className="sidebarIcon" />
                  {t("Button_1")}
                </button>
              </Link>
            </li>
            <li className="sidebarListItem">
              <Link to="/dashboards/universityanalytics">
                <button>
                  <Timeline className="sidebarIcon" />
                  {t("Button_2")}
                </button>
              </Link>
            </li>
            <li className="sidebarListItem">
              <Link to="/dashboards/globalanalytics">
                <button>
                  <MultilineChart className="sidebarIcon" />
                  {t("Button_3")}
                </button>
              </Link>
            </li>
          </ul>
          <div className="unit">
            <label htmlFor="unit"> {t("Unit_Analytics_Change")}: </label>
            <select
              id="unitId"
              name="unitId"
              value={unit.unitId}
              onChange={handleChange}
            >
              <option value="">{t("Select_Unit")}</option>
              {units?.map((unit) => (
                <option key={unit.idunit} value={unit.idunit}>
                  {unit.name}
                </option>
              ))}
            </select>
          </div>
          {text}
          <div className="row-container">
            <div className="small-chart">
              <img
                className="information"
                data-tip
                data-for="unit1Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit1Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("Unit_Graphic_1")}
              </ReactTooltip>
              <div
                className="gauge"
                style={{
                  textAlign: "center",
                  maxWidth: "600px",
                  maxHeight: "500px",
                  marginLeft: "4rem",
                  marginRight: "4rem",
                }}
              >
                <h4 className="chartTitle"> {t("Unit_Analytics_Text_1")} </h4>
                <div className="gaugeText">
                  <h4 style={{ color: "red" }}>{t("Low")}</h4>
                  <h4 style={{ color: "orange" }}>{t("Moderate")}</h4>
                  <h4 style={{ color: "green" }}>{t("High")}</h4>
                </div>
                <Gauge data={gauge ? Number(gauge.toFixed(2)) : 1} />
                <h4 className="chartText">
                  {t("Unit_Analytics_Text_2")}{" "}
                  {gauge ? Number(gauge.toFixed(2)) : 1} / 4
                </h4>
              </div>
            </div>
            <div className="small-chart">
              <img
                data-tip
                data-for="unit2Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit2Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("Unit_Graphic_2")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Pie Chart : */}
                {t("Unit_Analytics_Text_3")}
              </h4>
              <PieChart data={pie} />
            </div>
          </div>
          <div className="row-container">
            <div className="chart">
              <img
                data-tip
                data-for="unit3Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit3Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("Unit_Graphic_3")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Radar Chart :  */}
                {t("Unit_Analytics_Text_4")}
              </h4>
              <RadarChart data={radar} />
            </div>

            <div className="chart">
              <img
                data-tip
                data-for="unit4Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit4Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("Unit_Graphic_4")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Bar Chart :  */}
                {t("Unit_Analytics_Text_5")}
              </h4>
              <DistanceBarChart data={distBar} />
            </div>
          </div>
          <div className="row-container">
            <div className="chart">
              <img
                data-tip
                data-for="unit5Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit5Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("Unit_Graphic_5")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Horizontal Bar Chart :  */}
                {t("Unit_Analytics_Text_6")}
              </h4>
              <HorizontalChart data={hbar} />
            </div>

            <div className="chart">
              <img
                data-tip
                data-for="unit6Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit6Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("Unit_Graphic_6")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Bar Chart :  */}
                {t("Unit_Analytics_Text_6")}
                <BarChart data={bar} />
              </h4>
            </div>
          </div>
        </div>
      </div>
      <div className="big-container">
        <div className="charts-container">
          <div className="row-container">
            <div className="big-chart">
              <img
                data-tip
                data-for="unit7Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit7Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("Unit_Graphic_7")}
              </ReactTooltip>
              <h4 className="chartTitle">{t("Unit_Analytics_Text_7")} </h4>
              <form className="formdashboard">
                <label htmlFor="dateIn">{t("Unit_Analytics_Text_9")}:</label>
                <input
                  type="date"
                  
                  min="2021-01-01" 
                  max="2024-12-31"
                  name="dateIn"
                  id="dateIn"
                  placeholder={"YYYY-MM-DD"}
                  onChange={(e) => setDateIn(e.target.value)}
                />
                <label htmlFor="dateIn"> {t("Unit_Analytics_Text_10")}:</label>
                <input
                  type="date"
                  min="2021-01-01" 
                  max="2024-12-31"
                  name="dateOut"
                  id="dateOut"
                  placeholder={"YYYY-MM-DD"}
                  onChange={(e) => setDateOut(e.target.value)}
                />
              </form>

              <LineChart data={line} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UnitAnalytics;
