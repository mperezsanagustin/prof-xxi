import React, { useState, useEffect } from "react";
import "./Dashboards.css";
import { Link } from "react-router-dom";
import axios from "axios";
import PieChart from "./charts/PieChart";
import BarChart from "./charts/BarChart";
import HorizontalChart from "./charts/HorizontalBarChart";
import Gauge from "./charts/Gauge";
import RadarChart from "./charts/RadarChart";
import LineChart from "./charts/LineChart";
import DistanceBarChart from "./charts/DistanceBarChart";
import { Timeline, ShowChart, MultilineChart } from "@material-ui/icons";
import ReactTooltip from "react-tooltip";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function UniversityAnalytics() {
  const [unit1, setUnit1] = useState({
    num: "1",
    unitId: "",
    unitName: "",
    unitType: "",
    unitDescription: "",
    accessType: "",
  });

  const [buttonPopup, setButtonPopup] = useState(false);

  const [unit2, setUnit2] = useState({
    num: "2",
    unitId: "",
    unitName: "",
    unitType: "",
    unitDescription: "",
    accessType: "",
  });

  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );

  const [units, setUnits] = useState([]);
  const [text, setText] = useState("");

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${profile.token}`,
    },
  };

  const { t } = useTranslation();

  ////////////////////////////////////////////////////////////////////
  const fetchData = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/units/manageunit"
        : "http://localhost:3001/units/manageunit",
      config
    );
    setUnits(data.unitsResult);
  };

  const fetchCurrentUnits = async () => {
    console.log("Entering fetch Current");
    units.map((element) => {
      if (`${element.idunit}` === unit1.unitId) {
        setUnit1({
          ...unit1,
          unitId: unit1.unitId,
          unitName: element.name,
          unitType: element.type,
          unitDescription: element.description,
          accessType: element.access,
        });
      }
      if (`${element.idunit}` === unit2.unitId) {
        setUnit2({
          ...unit2,
          unitId: unit2.unitId,
          unitName: element.name,
          unitType: element.type,
          unitDescription: element.description,
          accessType: element.access,
        });
      }
    });
  };

  const handleChange1 = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUnit1({ ...unit1, [fieldName]: fieldValue });
  };

  const handleChange2 = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUnit2({ ...unit2, [fieldName]: fieldValue });
  };

  ////////////////////////////////////////////////////////////////////////

  const [gauge, setGauge] = useState({
    university: 1,
    unit1: 1,
    unit2: 1,
  });

  const fetchUniversityGauge = async () => {
    console.log(profile?.user?.organizationName);
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/universityanalysis/gauge/${profile?.user?.organizationName}`
        : `http://localhost:3001/dashboards/universityanalysis/gauge/${profile?.user?.organizationName}`,
      config
    );
    setGauge({ ...gauge, university: data.gaugeData });
  };

  const fetchGauge = async (unit) => {
    const response = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/gauge/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/gauge/${unit.unitId}`,
      config
    );
    if (unit.num === "1") {
      setGauge({ ...gauge, unit1: response.data.gaugeData });
    } else {
      setGauge({ ...gauge, unit2: response.data.gaugeData });
    }
  };

  /////////////////////////////////////////////////////////

  const [radar, setRadar] = useState({});

  const fetchUniversityRadar = async () => {
    const university = profile?.user?.organizationName;
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/universityanalysis/radar/${university}`
        : `http://localhost:3001/dashboards/universityanalysis/radar/${university}`,
      config
    );
    setRadar({
      ...gauge,
      university: { unitName: university, data: data.radarData },
    });
  };

  const fetchRadar = async (unit) => {
    const response = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/radar/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/radar/${unit.unitId}`,
      config
    );
    if (unit.num === "1") {
      setRadar({
        ...radar,
        unit1: { unitName: unit1.unitName, data: response.data.radarData },
      });
    } else {
      setRadar({
        ...radar,
        unit2: {
          unitName: unit2.unitName,
          data: response.data.radarData,
        },
      });
    }
  };
  ////////////////////////////////////////////////////////////////////
  const [distBar, setDistBar] = useState({});

  const fetchUniversityDistBar = async () => {
    const university = profile?.user?.organizationName;
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/universityanalysis/distbar/${university}`
        : `http://localhost:3001/dashboards/universityanalysis/distbar/${university}`,
      config
    );
    setDistBar({
      ...distBar,
      university: { unitName: university, data: data.distBarData },
    });
  };

  const fetchDistBar = async (unit) => {
    const response = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/distbar/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/distbar/${unit.unitId}`,
      config
    );
    if (unit.num === "1") {
      setDistBar({
        ...distBar,
        unit1: { unitName: unit1.unitName, data: response.data.distBarData },
      });
    } else {
      setDistBar({
        ...distBar,
        unit2: {
          unitName: unit2.unitName,
          data: response.data.distBarData,
        },
      });
    }
  };
  ////////////////////////////////////////////////////////////////////////
  const [bar, setBar] = useState({});

  const fetchBar = async (unit) => {
    const response = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/bar/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/bar/${unit.unitId}`,
      config
    );
    if (unit.num === "1") {
      setBar({
        ...bar,
        unit1: response.data.barData,
      });
    } else {
      setBar({
        ...bar,
        unit2: response.data.barData,
      });
    }
  };
  /////////////////////////////////////////////////////////////////
  const [hbar, setHbar] = useState({});

  const fetchHbar = async (unit) => {
    const response = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/dashboards/myunitsanalysis/hbar/${unit.unitId}`
        : `http://localhost:3001/dashboards/myunitsanalysis/hbar/${unit.unitId}`,
      config
    );
    if (unit.num === "1") {
      setHbar({
        ...hbar,
        unit1: response.data.hbarData,
      });
    } else {
      setHbar({
        ...hbar,
        unit2: response.data.hbarData,
      });
    }
  };
  ////////////////////////////////////////////////////////////////////
  const renderUnitAnalytics = async (unit) => {
    if (!(unit.unitId === "")) {
      await fetchGauge(unit);
      await fetchRadar(unit);
      await fetchDistBar(unit);
      await fetchBar(unit);
      await fetchHbar(unit);
    } else {
      if (unit.num === "1") {
        setGauge({ ...gauge, unit1: 1 });
        setRadar({ ...radar, unit1: {} });
        setDistBar({ ...distBar, unit1: {} });
        setBar({ ...bar, unit1: {} });
        setHbar({ ...hbar, unit1: {} });
      } else {
        setGauge({ ...gauge, unit2: 1 });
        setRadar({ ...radar, unit2: {} });
        setDistBar({ ...distBar, unit2: {} });
        setBar({ ...bar, unit2: {} });
        setHbar({ ...hbar, unit2: {} });
      }
    }
  };

  /////////////////////////////////////////////////////////

  useEffect(() => {
    document.title = "My University Analytics";
    setProfile(JSON.parse(localStorage.getItem("profile")));
    if (!profile?.token) {
      window.location = "/login";
    }
    fetchData();
    fetchUniversityGauge();
    fetchUniversityRadar();
    fetchUniversityDistBar();
  }, []);

  useEffect(() => {
    fetchCurrentUnits();
    renderUnitAnalytics(unit1);
  }, [unit1.unitId, unit1.unitName]);

  useEffect(() => {
    fetchCurrentUnits();
    renderUnitAnalytics(unit2);
  }, [unit2.unitId, unit2.unitName]);

  return (
    <div>
      <div className="container">
        <div className="charts-container">
          <ul className="sidebarList">
            <li className="sidebarListItem">
              <Link to="/dashboards/unitanalytics">
                <button>
                  <ShowChart className="sidebarIcon" />
                  {t("Button_1")}
                </button>
              </Link>
            </li>
            <li className="sidebarListItem">
              <Link to="/dashboards/universityanalytics">
                <button>
                  <Timeline className="sidebarIcon" />
                  {t("Button_2")}
                </button>
              </Link>
            </li>
            <li className="sidebarListItem">
              <Link to="/dashboards/globalanalytics">
                <button>
                  <MultilineChart className="sidebarIcon" />
                  {t("Button_3")}
                </button>
              </Link>
            </li>
          </ul>
          <div className="unit">
            <div>
              <div>
                <label htmlFor="unitId">
                  {" "}
                  {t("University_Analytics_Change")} 1 :{" "}
                </label>
                <select
                  id="unitId"
                  name="unitId"
                  value={unit1.unitId}
                  onChange={handleChange1}
                >
                  <option value=""> {t("Select_Unit")}</option>
                  {units?.map((unit) => (
                    <option key={unit.idunit} value={unit.idunit}>
                      {unit.name}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label htmlFor="unitId">
                  {" "}
                  {t("University_Analytics_Change")} 2 :{" "}
                </label>
                <select
                  id="unitId"
                  name="unitId"
                  value={unit2.unitId}
                  onChange={handleChange2}
                >
                  <option value=""> {t("Select_Unit")}</option>
                  {units?.map((unit) => (
                    <option key={unit.idunit} value={unit.idunit}>
                      {unit.name}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
          <div className="row-container">
            <div className="small-chart-univ">
              <img
                data-tip
                data-for="unit1Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit1Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_1")}
              </ReactTooltip>
              <div
                className="gauge"
                style={{
                  textAlign: "center",
                  maxWidth: "600px",
                  maxHeight: "500px",
                  marginLeft: "4rem",
                  marginRight: "4rem",
                }}
              >
                <h4 className="chartTitle">
                  {" "}
                  {t("University_Analytics_Text_1")} : {unit1.unitName}
                </h4>
                <div className="gaugeText">
                  <h4 style={{ color: "red" }}>{t("Low")}</h4>
                  <h4 style={{ color: "orange" }}>{t("Moderate")}</h4>
                  <h4 style={{ color: "green" }}>{t("High")}</h4>
                </div>
                <Gauge
                  data={gauge.unit1 ? Number(gauge.unit1.toFixed(2)) : 1}
                />
                <h5 className="chartText">
                  {t("University_Analytics_Text_2")} :{" "}
                  {gauge.unit1 ? Number(gauge.unit1.toFixed(2)) : 1} / 4
                </h5>
              </div>
            </div>
            <div className="small-chart-univ">
              <img
                data-tip
                data-for="unit2Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit2Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_1")}
              </ReactTooltip>
              <div
                className="gauge"
                style={{
                  textAlign: "center",
                  maxWidth: "600px",
                  maxHeight: "500px",
                  marginLeft: "4rem",
                  marginRight: "4rem",
                }}
              >
                <h4 className="chartTitle">
                  {" "}
                  {t("University_Analytics_Text_3")}{" "}
                </h4>
                <div className="gaugeText">
                  <h4 style={{ color: "red" }}>{t("Low")}</h4>
                  <h4 style={{ color: "orange" }}>{t("Moderate")}</h4>
                  <h4 style={{ color: "green" }}>{t("High")}</h4>
                </div>
                <Gauge
                  data={
                    gauge.university ? Number(gauge.university.toFixed(2)) : 1
                  }
                />
                <h5 className="chartText">
                  {t("University_Analytics_Text_4")} :
                  {gauge.university ? Number(gauge.university.toFixed(2)) : 1} /
                  4
                </h5>
              </div>
            </div>
            <div className="small-chart-univ">
              <img
                data-tip
                data-for="unit3Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit3Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_1")}
              </ReactTooltip>
              <div
                className="gauge"
                style={{
                  textAlign: "center",
                  maxWidth: "600px",
                  maxHeight: "500px",
                  marginLeft: "4rem",
                  marginRight: "4rem",
                }}
              >
                <h4 className="chartTitle">
                  {" "}
                  {t("University_Analytics_Text_5")} : {unit2.unitName}{" "}
                </h4>
                <div className="gaugeText">
                  <h4 style={{ color: "red" }}>{t("Low")}</h4>
                  <h4 style={{ color: "orange" }}>{t("Moderate")}</h4>
                  <h4 style={{ color: "green" }}>{t("High")}</h4>
                </div>
                <Gauge
                  data={gauge.unit2 ? Number(gauge.unit2.toFixed(2)) : 1}
                />
                <h5 className="chartText">
                  {t("University_Analytics_Text_6")} :{" "}
                  {gauge.unit2 ? Number(gauge.unit2.toFixed(2)) : 1} / 4
                </h5>
              </div>
            </div>
          </div>
          <div className="row-container">
            <div className="chart">
              <img
                data-tip
                data-for="unit4Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit4Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_2")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Radar Chart : */}
                {t("University_Analytics_Text_7")}
              </h4>
              <RadarChart
                data={radar?.unit1}
                data2={radar?.unit2}
                data3={radar?.university}
              />
            </div>

            <div className="chart">
              <img
                data-tip
                data-for="unit5Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit5Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_3")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Bar Chart :  */}
                {t("University_Analytics_Text_8")}
              </h4>
              <DistanceBarChart
                data={distBar?.unit1}
                data2={distBar?.unit2}
                data3={distBar?.university}
              />
            </div>
          </div>
          <div className="row-container">
            <div className="chart">
              <img
                data-tip
                data-for="unit6Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit6Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_4")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Bar Chart :  */}
                {t("University_Analytics_Text_9")} : {unit1.unitName}
              </h4>
              <BarChart data={bar?.unit1} />
            </div>

            <div className="chart">
              <img
                data-tip
                data-for="unit7Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit7Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_5")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Bar Chart :  */}
                {t("University_Analytics_Text_10")} :{unit2.unitName}
              </h4>
              <BarChart data={bar?.unit2} />
            </div>
          </div>
          <div className="row-container">
            <div className="chart">
              <img
                data-tip
                data-for="unit8Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit8Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_5")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Horizontal Bar Chart :  */}
                {t("University_Analytics_Text_11")}: {unit1.unitName}
              </h4>
              <HorizontalChart data={hbar?.unit1} />
            </div>

            <div className="chart">
              <img
                data-tip
                data-for="unit9Tip"
                src="/images/info_icon.png"
                width="30px"
                height="30px"
                alt=""
              />
              <ReactTooltip
                id="unit9Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("University_Graphic_5")}
              </ReactTooltip>
              <h4 className="charTitle">
                {/* Horizontal Bar Chart :  */}
                {t("University_Analytics_Text_12")}: {unit2.unitName}
              </h4>
              <HorizontalChart data={hbar?.unit2} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UniversityAnalytics;
