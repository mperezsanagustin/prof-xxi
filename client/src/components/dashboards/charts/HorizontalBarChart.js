import React from "react";
import { HorizontalBar } from "react-chartjs-2";
import i18n from "./../../../i18n";
import { useTranslation } from "react-i18next";

function HorizontalBarChart({ data }) {
  const { t } = useTranslation();

  return (
    <div>
      <HorizontalBar
        data={{
          labels: [t("1"), t("2"), t("3"), t("4"), t("5")],
          datasets: [
            {
              label: "Teachers",
              data: [
                data?.Teacher?.AVG_L1,
                data?.Teacher?.AVG_L2,
                data?.Teacher?.AVG_L3,
                data?.Teacher?.AVG_L4,
                data?.Teacher?.AVG_L5,
              ],
              backgroundColor: "rgba(255, 99, 132, 1)",
            },
            {
              label: "Administrators",
              data: [
                data?.Administrator?.AVG_L1,
                data?.Administrator?.AVG_L2,
                data?.Administrator?.AVG_L3,
                data?.Administrator?.AVG_L4,
                data?.Administrator?.AVG_L5,
              ],
              backgroundColor: "rgba(54, 162, 235, 1)",
            },
            {
              label: "Managers",
              data: [
                data?.Manager?.AVG_L1,
                data?.Manager?.AVG_L2,
                data?.Manager?.AVG_L3,
                data?.Manager?.AVG_L4,
                data?.Manager?.AVG_L5,
              ],
              backgroundColor: "rgba(255, 206, 86, 1)",
            },
            {
              label: "Students",
              data: [
                data?.Student?.AVG_L1,
                data?.Student?.AVG_L2,
                data?.Student?.AVG_L3,
                data?.Student?.AVG_L4,
                data?.Student?.AVG_L5,
              ],
              backgroundColor: "rgba(102, 205, 170, 1)",
            },
          ],
        }}
        height={500}
        width={500}
        options={{
          maintainAspectRatio: false,
          scales: {
            yAxes: [
              {
                ticks: {
                  min: 0,
                  max: 4,
                },
              },
            ],
          },
          legend: {
            labels: {
              fontSize: 12,
            },
          },
        }}
      />
    </div>
  );
}

export default HorizontalBarChart;
