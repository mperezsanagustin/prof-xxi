import React, { useEffect } from "react";
import "./About.css";
import Footer from "../../components/Footer";
import ReactPlayer from "react-player";
import { Link } from "react-router-dom";
import Manual from "./Documents/Manual.pdf";
import Pdf from "./Documents/Pdf.pdf";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function About() {
  const { t } = useTranslation();

  useEffect(() => {
    document.title = "About";
  }, []);

  return (
    <>
      <div className="container_0">
        <div className="paragraph">
          <p className="container_0_title"> {t("About_Title_1")} </p>
          <p className="container_0_text">{t("About_Paragraph_1")}</p>
        </div>
        <ReactPlayer
          className="container_0_video"
          width="450px"
          height="230px"
          url="https://www.youtube.com/watch?v=rz9RQmqWgvM"
        />
      </div>
      <div className="container_1">
        <div className="paragraph">
          <p className="container_1_title">{t("About_Title_2")} </p>
          <p className="container_1_text">
            {t("About_Paragraph_2")}
            <br></br>
            <div className="dims-container">
              {t("About_Dim_1")}
              <br></br>
              {t("About_Dim_2")}
              <br></br>
              {t("About_Dim_3")}
              <br></br>
              {t("About_Dim_4")}
              <br></br>
              {t("About_Dim_5")}
            </div>
            <br></br>
            {t("About_Paragraph_3")} <br></br>
            <div className="dims-container">
              {t("About_Level_1")}
              <br></br>
              {t("About_Level_2")}
              <br></br>
              {t("About_Level_3")}
              <br></br>
              {t("About_Level_4")}
              <br></br>
              {t("About_Level_5")}
              <br></br>
            </div>
          </p>
        </div>
        <img
          className="container_1_image1"
          src="/images/about1.png"
          width="450px"
          height="450px"
          alt=""
        />
      </div>
      <div className="container_2">
        <h2 className="container_2_title">{t("About_Title_2")}</h2>
        <ul className="container_2_boxes">
          <li className="container_2_box">{t("About_Option_1")}</li>
          <li className="container_2_box">{t("About_Option_2")}</li>
          <li className="container_2_box">{t("About_Option_3")}</li>
        </ul>
      </div>
      <div className="container_3">
        <h2 className="container_3_title">{t("About_Title_4")}</h2>
        <ul className="container_3_boxes">
          <li className="container_3_box">
            <p className="container_3_box_title">{t("About_Repo_1")}</p>
            <p className="container_3_box_text">{t("About_Repo_Pragraph_1")}</p>
            <a href="https://www.mdpi.com/2079-9292/11/3/413">
              <tr class="centred-btn">
                <button className="container_3_box_button">
                  {t("About_Button_1")}
                </button>
              </tr>
            </a>
          </li>
          <li className="container_3_box">
            <p className="container_3_box_title">{t("About_Repo_2")}</p>
            <p className="container_3_box_text">{t("About_Repo_Pragraph_2")}</p>
            <p className="container_3_box_text">{t("About_Repo_Pragraph_2")}</p>
            <a href={Pdf} target="_blank" rel="noreferrer">
              <tr class="centred-btn">
                <button className="container_3_box_button">
                  {t("About_Button_1")}
                </button>
              </tr>
            </a>
          </li>
          <li className="container_3_box">
            <p className="container_3_box_title">{t("About_Repo_3")}</p>
            <p className="container_3_box_text">{t("About_Repo_Pragraph_3")}</p>
            <a href={Manual} target="_blank" rel="noreferrer">
              <tr class="centred-btn">
                <button className="container_3_box_button">
                  {t("About_Button_1")}
                </button>
              </tr>
            </a>
          </li>
        </ul>
      </div>
      <Footer />
    </>
  );
}

export default About;
