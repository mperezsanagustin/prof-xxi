import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import FrontText from "../../components/FrontText";
import ReactPlayer from "react-player";
import Footer from "../../components/Footer";
import "./Home.css";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";
import { LangContext } from "./../contexts/LangContext";

function Home() {
  // Translation
  const { t } = useTranslation();
  const { lang, setLang } = useContext(LangContext);

  const handleTranslate = () => {
    i18n.changeLanguage(lang);
  };
  //

  useEffect(() => {
    document.title = "Home";
  }, []);

  return (
    <>
      <div className="part_1">
        <div className="paragraph">
          <p className="part_1_title">{t("Home_Title_1")}</p>
          <p className="part_1_text">{t("Home_Text_1")}</p>
        </div>
        <img className="part_1_image" src="/images/home_page.png" alt="" />
      </div>
      <div className="part_2">
        <img className="part_2_image" src="/images/part1.png" alt="" />
        <div className="paragraph">
          <p className="part_2_title">{t("Home_Title_2")}</p>
          <p className="part_2_text">{t("Home_Text_2")}</p>
        </div>
      </div>
      <div className="part_3">
        <h2 className="part_3_title">{t("Home_Title_3")} </h2>
        <ul className="part_3_boxes">
          <li className="part_3_box">{t("Home_Element_1")}</li>
          <li className="part_3_box">{t("Home_Element_2")}</li>
          <li className="part_3_box">{t("Home_Element_3")}</li>
          <li className="part_3_box">{t("Home_Element_4")}</li>
        </ul>
      </div>
      {/* <ReactPlayer
            className="element"
            height="500px"
            width="800px"
            url="https://www.youtube.com/watch?v=rz9RQmqWgvM"
          /> */}
      <Footer />
    </>
  );
}

export default Home;
