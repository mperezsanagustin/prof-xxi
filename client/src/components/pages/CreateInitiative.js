import Axios from "axios";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./Login.css";
import "./Initiatives.css";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";
import ReactTooltip from "react-tooltip";

function CreateInitiative() {
  const [initiative, setInitiative] = useState({
    initiativeName: "",
    initiativeDescription: "",
    initiativeTechnologies: "",
    initiativeTime: "",
    accessType: "Public",
    dimA: false,
    dimB: false,
    dimC: false,
    dimD: false,
    dimE: false,
  });

  const [text, setText] = useState("");
  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );

  const { t } = useTranslation();

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setInitiative({ ...initiative, [fieldName]: fieldValue });
  };

  const handleCheckBox = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.checked;

    setInitiative({ ...initiative, [fieldName]: fieldValue });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(initiative);
    if (profile) {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${profile.token}`,
        },
      };

      if (initiative.initiativeName === "") {
        setText("Create_Initiative_Validation_4");
      } else {
        Axios.post(
          process.env.REACT_APP_ENV === "production"
            ? "/api/initiatives/createinitiative"
            : "http://localhost:3001/initiatives/createinitiative",
          { initiative: initiative },
          config
        ).then((response) => {
          if (response.data.created) {
            setText("Create_Initiative_Validation_1");
          } else if (response.data?.status === "existing") {
            setText("Create_Initiative_Validation_3");

            setText(response.data.message);
          } else {
            setText("Create_Initiative_Validation_2");
          }
        });
      }
    }
  };

  useEffect(() => {
    document.title = "Create your initiative";
    console.log(profile);
    if (!profile?.token) {
      window.location = "/login";
    }
  }, []);

  return (
    <>
      <div className="box">
        <img
          className="sideimg"
          width="50%"
          height="1200px"
          src="images/photo6.jpg"
          alt=""
        />
        <article className="form">
          <form>
            <div className="title">
              <p> {t("Create_Initiative_Title_1")} </p>
            </div>
            <div>
              <label htmlFor="initiativeName">
                {" "}
                {t("Create_Initiative_Title_2")}:
              </label>
              <input
                type="text"
                id="initiativeName"
                name="initiativeName"
                value={initiative.name}
                onChange={handleChange}
                placeholder={t("45")}
              />
            </div>

            <label htmlFor="initiativeDescription">
              {t("Create_Initiative_Title_3")}:
            </label>

            <div>
              <textarea
                name="initiativeDescription"
                id="initiativeDescription"
                value={initiative.initiativeDescription}
                onChange={handleChange}
                placeholder={t("300")}
                cols="50"
                rows="7"
              ></textarea>
            </div>

            <div>
              <label htmlFor="dimA"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimA"
                  name="dimA"
                  value="dimA"
                  checked={initiative.dimA}
                  onChange={handleCheckBox}
                />
                <span className="check_text_dim" style={{ color: "#224E9F" }}>
                  {t("Create_Initiative_A")}{" "}
                </span>
              </div>

              <label htmlFor="dimB"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimB"
                  name="dimB"
                  value="dimB"
                  checked={initiative.dimB}
                  onChange={handleCheckBox}
                />
                <span className="check_text_dim" style={{ color: "#224E9F" }}>
                  {t("Create_Initiative_B")}
                </span>
              </div>

              <label htmlFor="dimC"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimC"
                  name="dimC"
                  value="dimC"
                  checked={initiative.dimC}
                  onChange={handleCheckBox}
                />
                <span className="check_text_dim" style={{ color: "#224E9F" }}>
                  {t("Create_Initiative_C")}
                </span>
              </div>

              <label htmlFor="dimD"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimD"
                  name="dimD"
                  value="dimD"
                  checked={initiative.dimD}
                  onChange={handleCheckBox}
                />
                <span className="check_text_dim" style={{ color: "#224E9F" }}>
                  {t("Create_Initiative_D")}
                </span>
              </div>

              <label htmlFor="dimE"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimE"
                  name="dimE"
                  value="dimE"
                  checked={initiative.dimE}
                  onChange={handleCheckBox}
                />
                <span className="check_text_dim" style={{ color: "#224E9F" }}>
                  {t("Create_Initiative_E")}
                </span>
              </div>

              <label htmlFor="initiativeTechnologies">
                {t("Create_Initiative_Title_4")}:
              </label>
              <div>
                <input
                  type="text"
                  id="initiativeTechnologies"
                  name="initiativeTechnologies"
                  value={initiative.initiativeTechnologies}
                  onChange={handleChange}
                  placeholder={t("25")}
                />
              </div>
            </div>

            <div>
              <label htmlFor="accessType">
                {" "}
                {t("Create_Initiative_Title_5")}:
              </label>
              <select
                id="accessType"
                name="accessType"
                value={initiative.accessType}
                onChange={handleChange}
              >
                <option value="Public">{t("Create_Unit_Public")}</option>
                <option value="Private">{t("Create_Unit_Private")}</option>
              </select>
            </div>

            <div>
              <tr class="centred-btn">
                <button type="submit" className="btn" onClick={handleSubmit}>
                  {t("Create_Initiative_Button_1")}
                </button>
              </tr>
              <Link to="/workingarea">
                <tr class="centred-btn">
                  <button className="btn">
                    {" "}
                    {t("Create_Initiative_Button_2")}
                  </button>
                </tr>
              </Link>
            </div>
            <tr class="centred-btn">
              <h5 style={{ color: "red", marginTop: "2rem" }}>{t(text)}</h5>
            </tr>
          </form>
        </article>
      </div>
    </>
  );
}

export default CreateInitiative;
