import React from "react";
import { Link } from "react-router-dom";
import "./Default.css";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function ScansInformation() {
  const { t } = useTranslation();

  return (
    <>
      <h2>{t("Scan_Ques_Info_Title")}</h2>
      <div className="middle-container">
        <div>
          <h3>{t("Scan_1_Ques_Info_Title_1")}</h3>
          <div className="item-area-2">
            <ul style={{ padding: "1.5rem" }}>
              <li>
                <p className="scan-title"> {t("Scan_1_Ques_Info_Title_2")}:</p>
                <p className="scan-text">{t("Scan_1_Ques_Info_Paragraph_1")}</p>
              </li>
              <li>
                <p className="scan-title">{t("Scan_1_Ques_Info_Title_3")}:</p>
                <p className="scan-text">
                  {" "}
                  {t("Scan_1_Ques_Info_Paragraph_2")}{" "}
                </p>
              </li>
              <li>
                <p className="scan-title">{t("Scan_1_Ques_Info_Title_4")}</p>
                <p className="scan-text">{t("Scan_1_Ques_Info_Paragraph_3")}</p>
              </li>
              <li>
                <p className="scan-title">{t("Scan_1_Ques_Info_Title_5")} </p>
                <p className="scan-text">{t("Scan_1_Ques_Info_Paragraph_4")}</p>
              </li>
              <Link to="/scangenerator">
                <button className="btn">
                  {" "}
                  {t("Scan_1_Ques_Info_Button_1")}
                </button>
              </Link>
            </ul>
          </div>
          <h3>{t("Scan_2_Ques_Info_Title_1")}</h3>
          <div className="item-area-2">
            <ul style={{ padding: "1.5rem" }}>
              <li>
                <p className="scan-title"> {t("Scan_2_Ques_Info_Title_2")}:</p>
                <p className="scan-text">{t("Scan_2_Ques_Info_Paragraph_1")}</p>
              </li>
              <li>
                <p className="scan-title"> {t("Scan_2_Ques_Info_Title_3")}:</p>
                <p className="scan-text">
                  {" "}
                  {t("Scan_2_Ques_Info_Paragraph_2")}{" "}
                </p>
              </li>
              <li>
                <p className="scan-title">{t("Scan_2_Ques_Info_Title_4")} </p>
                <p className="scan-text">
                  {t("Scan_2_Ques_Info_Paragraph_3")}{" "}
                </p>
              </li>
              <li>
                <p className="scan-title">{t("Scan_2_Ques_Info_Title_5")} </p>
                <p className="scan-text">{t("Scan_2_Ques_Info_Paragraph_4")}</p>
              </li>
              <Link to="/scangenerator">
                <button className="btn">
                  {t("Scan_2_Ques_Info_Button_1")}
                </button>
              </Link>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
}

export default ScansInformation;
