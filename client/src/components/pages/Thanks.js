import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import Footer from "../../components/Footer";
import "./Default.css";
import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function Thanks() {
  const { t } = useTranslation();

  useEffect(() => {
    document.title = "Thank You";
  }, []);

  return (
    <>
      <div className="box">
        <img src="images/design9.jpg" width="50%" height="1000px" alt="" />
        <article className="form">
          <div>
            <div className="title">
              <p>{t("Thanks_Title")} </p>
            </div>
            <p className="thanks">{t("Thanks_Text_1")}</p>
            <p className="thanks">{t("Thanks_Text_2")}</p>
          </div>
        </article>
      </div>
      <Footer></Footer>
    </>
  );
}

export default Thanks;
