import React, { useState, useEffect, useContext } from "react";
import Axios from "axios";
import "./Login.css";
import Footer from "../../components/Footer";
import { Link, Redirect, useHistory } from "react-router-dom";
import WorkingArea from "../../components/pages/WorkingArea";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();
  const [loginState, setloginState] = useState("");
  const [forgot, setForgot] = useState({ state: false, email: "" });
  const [text, setText] = useState("");

  const { t } = useTranslation();

  Axios.defaults.withCredentials = true;

  const login = (e) => {
    e.preventDefault();
    Axios.post(
      process.env.REACT_APP_ENV === "production"
        ? "/api/user/login"
        : "http://localhost:3001/user/login",
      {
        email: email,
        password: password,
      }
    ).then((response) => {
      console.log(response);

      if (response.data.loggedIn) {
        setloginState(response.data.message);
        localStorage.setItem("profile", JSON.stringify(response.data));
        window.location = "/workingarea";
      }

      setText(response.data.message);

      //   history.push("/workingarea");
    });
  };

  const handleForgotRequest = (e) => {
    e.preventDefault();
    setForgot({ ...forgot, state: !forgot.state });
  };

  const handleForgot = (e) => {
    const host = window.location.hostname;

    e.preventDefault();
    Axios.post(
      process.env.REACT_APP_ENV === "production"
        ? "/api/user/forgotpassword"
        : "http://localhost:3001/user/forgotpassword",
      {
        email: forgot.email,
        host: host,
      }
    ).then((response) => {
      if (response.data?.sent) {
        setText(response.data.message);
      } else {
        setText(response.data.message);
      }
    });
  };

  useEffect(() => {
    document.title = "Login";
    let profile = JSON.parse(localStorage.getItem("profile"));
    console.log(process.env.REACT_APP_ENV === "production");
    console.log(process.env.REACT_APP_ENV);
    if (profile) {
      window.location = "/workingarea";
    }
  }, []);

  return (
    <>
      <div className="box">
        <img
          className="sideimg"
          width="50%"
          height="900px"
          src="images/photo1.jpg"
          alt=""
        />
        <article className="form">
          <form>
            <img
              className="user"
              width="20%"
              height="20%"
              src="images/user.png"
              alt=""
            />
            <tr class="centred-btn">
              <Link to="/register">
                <h5 style={{ color: "red" }}>{t("Login_Text")} </h5>
              </Link>
            </tr>
            <div className="inputs">
              <div>
                <label htmlFor="email">{t("Login_Email")}:</label>
                <input
                  type="email"
                  // placeholder="Email"
                  name="email"
                  id="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>

              <div>
                <label htmlFor="password"> {t("Login_Pass")}:</label>
                <input
                  type="password"
                  // placeholder="Password"
                  name="password"
                  id="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
            </div>
            <div>
              <tr class="centred-btn">
                <button type="submit" className="btn" onClick={login}>
                  {t("Login_Button_Login")}
                </button>
              </tr>
              {/* <tr class="centred-btn">
                <button
                  className="btn"
                  onClick={() => setForgot({ ...forgot, state: !forgot.state })}
                >
                  {t("Login_Button_Forgot")}{" "}
                </button>
              </tr> */}
            </div>
            <div>
              <tr class="centred-btn">
                <button className="btn" onClick={handleForgotRequest}>
                  {t("Login_Button_Forgot")}{" "}
                </button>
              </tr>
            </div>
            {forgot.state && (
              <div>
                <p style={{ color: "#224E9F" }}>
                  Please enter the email you registred with:
                </p>
                <form>
                  <div>
                    <label htmlFor="email">{t("Login_Email")}:</label>
                    <input
                      type="email"
                      // placeholder="Email"
                      name="email"
                      id="email"
                      value={forgot.email}
                      onChange={(e) =>
                        setForgot({ ...forgot, email: e.target.value })
                      }
                    />
                  </div>
                  <tr class="centred-btn">
                    <button className="btn" onClick={handleForgot}>
                      Request Password Resetting
                    </button>
                  </tr>
                </form>
              </div>
            )}
            <tr class="centred-btn">
              <h5 style={{ color: "red", marginTop: "2rem" }}>{t(text)}</h5>
            </tr>
          </form>
        </article>
      </div>
      <Footer />
    </>
  );
}

export default Login;
