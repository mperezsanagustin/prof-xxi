import React, { useState, useEffect } from "react";
import "./Login.css";
import { Link } from "react-router-dom";
import axios from "axios";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";



function ScanGenerator() {
  const [scan, setScan] = useState({
    unitId: "",
    scanType: "basic",
    status: "OPEN",
  });

  const [units, setUnits] = useState([]);
  const [loading, setLoading] = useState(false);
  const [text, setText] = useState("");
  const [link, setLink] = useState("");

  const { t } = useTranslation();

  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${profile.token}`,
    },
  };

  const fetchData = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/units/manageunit"
        : "http://localhost:3001/units/manageunit",
      config
    );
    setUnits(data.unitsResult);
    setLoading(true);
    console.log(units);
  };

  useEffect(() => {
    document.title = "Scan generator";
    setProfile(JSON.parse(localStorage.getItem("profile")));
    if (!profile?.token) {
      window.location = "/login";
    }
    fetchData();
  }, [loading]);

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setScan({ ...scan, [fieldName]: fieldValue });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(scan);
    const host = window.location.hostname;

    if (!(scan.unitId === "")) {
      const { data } = await axios.post(
        process.env.REACT_APP_ENV === "production"
          ? `/api/links/generatelink`
          : `http://localhost:3001/links/generatelink`,
        { unitId: scan.unitId, scanType: scan.scanType, host: host },
        config
      );

      setText(data.message);

      if (data?.link) {
        setLink(data.link);
      }
    } else {
      setText("Manage_Unit_Validation_3");
    }
  };

  return (
    <>
      <div className="box">
        <img
          className="sideimg"
          width="50%"
          height="700px"
          src="images/photo5.jpg"
          alt=""
        />
        <article className="form">
          <form>
            <div className="title">
              <p>{t("Scan_Link_Title_1")}</p>
            </div>
            <div>
              <label htmlFor="unit">{t("Scan_Link_Unit")}:</label>
              <select
                name="unitId"
                id="unitId"
                value={scan.unitId}
                onChange={handleChange}
              >
                <option value={null}>{t("Select_Unit")} </option>
                {units?.map((unit) => (
                  <option key={unit.idunit} value={unit.idunit}>
                    {unit.name}
                  </option>
                ))}
              </select>
            </div>

            <div>
              <label htmlFor="type"> {t("Scan_Link_Type")}:</label>
              <select
                name="scanType"
                id="scanType"
                value={scan.scanType}
                onChange={handleChange}
              >
                <option value="basic"> {t("Basic_Type")} </option>
                <option value="student">{t("Student_Type")}</option>
              </select>
            </div>

            <div>
              <label htmlFor="status"> {t("Scan_Link_Status")}:</label>
              <select
                id="status"
                name="status"
                value={scan.status}
                onChange={handleChange}
              >
                <option value="OPEN"> {t("Active")} </option>
                <option value="CLOSED"> {t("Not_Active")} </option>
              </select>
            </div>

            <div>
              <tr class="centred-btn">
                <button type="submit" className="btn" onClick={handleSubmit}>
                  {t("Scan_Link_Button_1")}
                </button>
              </tr>
            </div>
            <div>
              <tr class="centred-btn">
                <h5 style={{ color: "red", marginTop: "2rem" }}>{t(text)}</h5>
              </tr>
              {/* <h5 style={{ color: "red" }}>{link}</h5> */}
              <div style={{ marginTop: "1rem" }}>
                <a href={link}>{link}</a>
              </div>
            </div>
          </form>
          {/* <div>
            <p style={{ color: "red", marginTop: "2rem" }}>{text}</p>
            <p style={{ color: "red", marginTop: "2rem" }}>{link}</p>
          </div> */}
        </article>
      </div>
      {/* <Footer></Footer> */}
    </>
  );
}

export default ScanGenerator;
