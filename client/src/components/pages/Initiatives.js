import React, { useState, useEffect } from "react";
import Axios from "axios";
import "./Initiatives.css";
import { Link } from "react-router-dom";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";
import Footer from "../../components/Footer";

function Initiatives() {
  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );
  const { t } = useTranslation();

  useEffect(() => {
    document.title = "Initiatives";
    setProfile(JSON.parse(localStorage.getItem("profile")));
  }, []);

  // function track() {
  //   const newNote = {
  //   }
  //   Axios.post("http://localhost:3001/tracking/send",newNote)
  //   console.log("////////////////////////////////////////////test");
  // }

  return (
    <>
      <div className="Initiatives1">
        <p className="Initiatives1_title"> {t("Initiative_Title_1")}</p>
        <br></br>
        <p className="Initiatives1_text"> {t("Initiative_Text_0")}</p>
        {/* <Link to="/searchinitiatives">
          <button className="Initiatives1_btn">See more</button>
        </Link> */}
      </div>
      <div className="Initiatives2">
        <div>
          <p className="Initiatives2_title1"> {t("Initiative_Title_2")} </p>
          <div className="Initiatives2_box1">
            <p className="Initiatives2_text1">{t("Initiative_Text_1")}</p>
            <Link to="/searchinitiatives">
              <button className="Initiatives2_btn">
                {" "}
                {t("Initiative_Button_1")}
              </button>
            </Link>
          </div>
        </div>
        {profile ? (
          <div>
            <p className="Initiatives2_title2"> {t("Initiative_Title_3")} </p>
            <div className="Initiatives2_box2">
              <p className="Initiatives2_text2">{t("Initiative_Text_2")}</p>
              <Link to="/createinitiative">
                <button style={{ width: "240px" }} className="Initiatives2_btn">
                  {t("Initiative_Button_2")}
                </button>
              </Link>
              <Link to="/manageinitiative">
                <button style={{ width: "240px" }} className="Initiatives2_btn">
                  {t("Initiative_Button_3")}
                </button>
              </Link>
            </div>
          </div>
        ) : (
          <></>
        )}
      </div>
      <Footer />
    </>
  );
}

export default Initiatives;
