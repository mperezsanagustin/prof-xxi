import React, { useState, useEffect } from "react";
import axios from "axios";
import "./Login.css";
import ReactTooltip from "react-tooltip";
import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function UpdateProfile() {
  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );

  const [user, setUser] = useState({
    organizationName: profile?.user?.organizationName,
    universityWebsite: profile?.user?.universityWebsite,
    firstName: profile?.user?.firstName,
    lastName: profile?.user?.lastName,
    position: profile?.user?.position,
    profile: profile?.user?.profile,
    email: profile?.user?.email,
    password: profile?.user?.password,
  });

  const [resetPassword, setResetPassword] = useState({
    password: "",
    confirmedPassword: "",
  });

  const { t } = useTranslation();

  const [toggleConfirmation, setToggleConfirmation] = useState(false);
  const [toggleReset, setToggleReset] = useState(false);
  const [action, setAction] = useState("");
  const [text, setText] = useState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${profile.token}`,
    },
  };

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUser({ ...user, [fieldName]: fieldValue });
  };

  const handlePasswordChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setResetPassword({ ...resetPassword, [fieldName]: fieldValue });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(user);

    const { data } = await axios.put(
      process.env.REACT_APP_ENV === "production"
        ? `/api/user/update`
        : `http://localhost:3001/user/update`,
      { email: user.email, user: user },
      config
    );
    localStorage.setItem(
      "profile",
      JSON.stringify({ loggedIn: true, token: profile.token, user: data.user })
    );
    setText(data.message);
  };

  const reset = async (e) => {
    e.preventDefault();
    if (resetPassword.password === resetPassword.confirmedPassword) {
      const { data } = await axios.put(
        process.env.REACT_APP_ENV === "production"
          ? `/api/user/resetpassword`
          : `http://localhost:3001/user/resetpassword`,
        {
          password: resetPassword.password,
        },
        config
      );
      setText(data.message);
    } else {
      setText("Update_Profile_Validation_5");
    }
  };

  const handleDelete = () => {
    if (action === "data") {
      handleDeleteAllData();
    } else {
      handleDeleteProfile();
    }
  };
  const handleDeleteProfile = async () => {
    const { data } = await axios.delete(
      process.env.REACT_APP_ENV === "production"
        ? `/api/user/deleteprofile`
        : `http://localhost:3001/user/deleteprofile`,
      config,
      {}
    );
    setText(data.message);
    localStorage.removeItem("profile");
    window.location = "/login";
  };

  const handleDeleteAllData = async () => {
    const { data } = await axios.delete(
      process.env.REACT_APP_ENV === "production"
        ? `/api/user/deletedata`
        : `http://localhost:3001/user/deletedata`,
      config,
      {}
    );
    setText(data.message);
  };

  const handleConfirmation = (e) => {
    e.preventDefault();
    if (e.target.value === "data") {
      setAction("data");
    } else {
      setAction("profile");
    }

    if (!toggleConfirmation) {
      setToggleConfirmation(true);
      setText("Update_Profile_Validation_10");
    } else {
      setToggleConfirmation(false);
      setText("");
    }
  };

  const handleToggleReset = (e) => {
    e.preventDefault();
    setToggleReset((e) => !e);
  };

  useEffect(() => {
    document.title = "Update Profile";
    setProfile(JSON.parse(localStorage.getItem("profile")));
    if (!profile?.token) {
      window.location = "/login";
    }
  }, []);

  return (
    <>
      <dev className="title"></dev>
      <div className="box">
        <img
          className="sideimg"
          width="50%"
          height="1400px"
          src="images/update_profile.jpg"
          alt=""
        />
        <article className="form">
          <form>
            <div className="title">
              <p> {t("Update_Profile_Title")} </p>
            </div>
            <div>
              <label htmlFor="organizationName">{t("Register_Name")}: </label>
              <input
                data-tip
                data-for="registerTip"
                type="text"
                id="organizationName"
                name="organizationName"
                value={user.organizationName}
                onChange={handleChange}
              />
              <ReactTooltip id="registerTip" place="bottom" effect="solid">
                {t("Update_Profile_Text")}
              </ReactTooltip>
            </div>

            <div>
              <label htmlFor="universityWebsite">
                {t("Register_Website")}{" "}
              </label>
              <input
                type="text"
                id="universityWebsite"
                name="universityWebsite"
                value={user.universityWebsite}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="firstName">{t("Register_First Name")}:</label>
              <input
                type="text"
                id="firstName"
                name="firstName"
                value={user.firstName}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="lastName">{t("Register_Last Name")}:</label>
              <input
                type="text"
                id="lastName"
                name="lastName"
                value={user.lastName}
                onChange={handleChange}
              />
            </div>

            <div>
              <label htmlFor="position">{t("Register_Position")}:</label>
              <select
                name="position"
                id="position"
                value={user.position}
                onChange={handleChange}
              >
                <option value="Teacher">{t("Teacher")}</option>
                <option value="Administrator"> {t("Administrator")}</option>
                <option value="Manager">{t("Manager")}</option>
              </select>
            </div>

            <div>
              <label htmlFor="profile">{t("Register_Profile")}:</label>
              <input
                type="text"
                id="profile"
                name="profile"
                value={user.profile}
                onChange={handleChange}
              />
              {/* <p style={{ color: "blue" }}>
                Examples : LinkedIn / researchGate / University profile
              </p> */}
            </div>

            <div>
              <label htmlFor="email">{t("Register_Email")}:</label>
              <input
                type="email"
                id="email"
                name="email"
                value={user.email}
                onChange={handleChange}
              />
            </div>
            <tr class="centred-btn">
              <button type="submit" className="btn" onClick={handleSubmit}>
                {t("Update_Profile_Button_7")}
              </button>
            </tr>
            <tr class="centred-btn">
              <h5 style={{ color: "red", marginTop: "2rem" }}>{t(text)}</h5>
            </tr>
            {/* <h4 style={{ color: "red", marginTop: "2rem" }}>{text}</h4> */}

            <div>
              <tr class="centred-btn">
                <button
                  style={{ width: "250px" }}
                  className="btn"
                  onClick={handleConfirmation}
                  value="profile"
                >
                  {t("Update_Profile_Button_1")}
                </button>
              </tr>
              <tr class="centred-btn">
                <button
                  style={{ width: "250px" }}
                  className="btn"
                  onClick={handleConfirmation}
                  value="data"
                >
                  {t("Update_Profile_Button_2")}
                </button>
              </tr>
              <tr class="centred-btn">
                <button
                  style={{ width: "250px" }}
                  className="btn"
                  onClick={handleToggleReset}
                >
                  {t("Update_Profile_Button_3")}
                </button>
              </tr>
            </div>
            {toggleConfirmation && (
              <>
                <div class="centred-btn">
                  <button
                    style={{ backgroundColor: "red" }}
                    className="btn"
                    onClick={handleDelete}
                  >
                    {t("Update_Profile_Button_4")}
                  </button>
                  <button
                    style={{ backgroundColor: "orange" }}
                    className="btn"
                    onClick={handleConfirmation}
                  >
                    {t("Update_Profile_Button_5")}
                  </button>
                </div>
              </>
            )}

            {toggleReset && (
              <>
                <div>
                  <label htmlFor="password">
                    {" "}
                    {t("Update_Profile_Text_2")}:
                  </label>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    onChange={handlePasswordChange}
                  />
                </div>
                <div>
                  <label htmlFor="confirmedpassword">
                    {t("Update_Profile_Text_3")}:
                  </label>
                  <input
                    type="password"
                    id="confirmedPassword"
                    name="confirmedPassword"
                    onChange={handlePasswordChange}
                  />
                </div>
                <button className="btn" onClick={reset}>
                  {t("Update_Profile_Button_8")}
                </button>
              </>
            )}
          </form>

          {/* {toggleConfirmation && (
              <>
                <div>
                  <button
                    style={{ backgroundColor: "red" }}
                    className="btn"
                    onClick={handleDelete}
                  >
                    {t("Update_Profile_Button_4")}
                  </button>
                  <button
                    style={{ backgroundColor: "orange" }}
                    className="btn"
                    onClick={handleConfirmation}
                  >
                    {t("Update_Profile_Button_5")}
                  </button>
                </div>
              </>
            )}

            {toggleReset && (
              <>
                <div>
                  <label htmlFor="password">
                    {" "}
                    {t("Update_Profile_Text_2")} :
                  </label>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    onChange={handlePasswordChange}
                  />
                </div>
                <div>
                  <label htmlFor="confirmedpassword">
                    {t("Update_Profile_Text_3")} :
                  </label>
                  <input
                    type="password"
                    id="confirmedPassword"
                    name="confirmedPassword"
                    onChange={handlePasswordChange}
                  />
                </div>
                <button className="btn" onClick={reset}>
                  {t("Update_Profile_Button_8")}
                </button>
              </>
            )}
            <h5 className="warning">{text}</h5> */}
        </article>
      </div>
    </>
  );
}

export default UpdateProfile;
