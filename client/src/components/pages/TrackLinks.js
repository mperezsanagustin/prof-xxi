import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./Default.css";
import "./Tracking.css";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

import axios from "axios";

function TrackLinks() {
  const [questionnaires, setQuestionnaires] = useState([]);
  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );

  const { t } = useTranslation();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${profile.token}`,
    },
  };

  const fetchData = async () => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? `/api/links/tracklinks`
        : `http://localhost:3001/links/tracklinks`,
      config
    );
    setQuestionnaires(data.linksResult);
    console.log(questionnaires);
  };

  const handleStatus = async (e) => {
    e.preventDefault();
    const idLink = e.target.value;
    const { data } = await axios.put(
      process.env.REACT_APP_ENV === "production"
        ? "/api/links/updatelink"
        : "http://localhost:3001/links/updatelink",
      { idLink: idLink },
      config
    );
    console.log(data);
    window.location.reload();
  };

  useEffect(() => {
    document.title = "Track your links";
    if (!profile?.token) {
      window.location = "/login";
    }
    fetchData();
  }, []);

  return (
    <>
      <h2>{t("Track_Links_Title")}</h2>
      <div className="table-container">
        <div>
          <table className="track">
            <tr>
              <th>{t("Track_Links_Link")}</th>
              <th>{t("Track_Links_Name")}</th>
              <th>{t("Track_Links_Date")}</th>
              <th>{t("Track_Links_Type")}</th>
              <th>{t("Track_Links_Status")}</th>
            </tr>

            {questionnaires.map((element) => {
              return (
                <tr key={element.link}>
                  <td>{element.link}</td>
                  <td>{element.name}</td>
                  <td>{element.date}</td>
                  <td>
                    {element.type === "basic"
                      ? t("Basic_Type")
                      : t("Student_Type")}{" "}
                  </td>
                  <td>
                    {element.status === "OPEN" ? t("Active") : t("Not_Active")}
                    <button
                      className="btn"
                      onClick={handleStatus}
                      value={element.idquestionnaire}
                    >
                      {element.status === "OPEN" ? "Disable" : "Enable"}
                    </button>
                  </td>
                </tr>
              );
            })}
          </table>
        </div>
        <Link to="/workingarea">
          <button className="btn"> {t("Track_Links_Button_1")}</button>
        </Link>
      </div>

      {/* <Footer /> */}
    </>
  );
}

export default TrackLinks;
