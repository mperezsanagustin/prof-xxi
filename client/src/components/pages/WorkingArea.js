import React, { useState, useEffect, useContext } from "react";
import { Link, useLocation } from "react-router-dom";
import Axios from "axios";
import "./WorkingArea.css";
import axios from "axios";
import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function WorkingArea() {
  const [value, setValue] = useState(0);
  const [unitsNumber, setUnitsNumber] = useState("0");
  const [scansNumber, setScansNumber] = useState("0");
  const [participantsNumber, setParticipantsNumber] = useState("0");
  const [totalUnits, setTotalUnits] = useState("0");
  const [totalParticipants, setTotalParticipants] = useState("0");
  const [totalUniversities, setTotalUniversities] = useState("0");
  const [totalScans, setTotalScans] = useState("0");

  //const [initiativesNumber, setInitiativesNumber] = useState("0");
  const [initiativesNumber, setInitiativesNumber] = useState("0");

  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );
  const [text, setText] = useState("");
  const location = useLocation();

  const { t } = useTranslation();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${profile?.token}`,
    },
  };

  ////////////////////// GETTING WORKING AREA STATS ///////////////////////
  const getUnitsNumber = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/units/unitsnumber"
        : "http://localhost:3001/units/unitsnumber",
      config
    );
    setUnitsNumber(data.unitsNumber);
  };

  const getScansNumber = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/links/scansnumber"
        : "http://localhost:3001/links/scansnumber",
      config
    );
    setScansNumber(data.scansNumber);
  };

  const getParticipantsNumber = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/links/participantsnumber"
        : "http://localhost:3001/links/participantsnumber",
      config
    );
    setParticipantsNumber(data.participantsNumber);
  };

  const getTotalUnitsNumber = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/units/totalunitsnumber"
        : "http://localhost:3001/units/totalunitsnumber",
      config
    );
    setTotalUnits(data.totalUnitsNumber);
  };

  const getTotalParticipantsNumber = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/links/totalparticipantsnumber"
        : "http://localhost:3001/links/totalparticipantsnumber",
      config
    );
    setTotalParticipants(data.totalParticipantsNumber);
  };

  const getTotalUniversitiesNumber = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/user/totaluniversitiesnumber"
        : "http://localhost:3001/user/totaluniversitiesnumber",
      config
    );
    setTotalUniversities(data.totalUniversitiesNumber);
  };

  const getTotalScansNumber = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/links/totalscansnumber"
        : "http://localhost:3001/links/totalscansnumber",
      config
    );
    setTotalScans(data.totalScansNumber);
  };

  const getInitiativesNumber = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/initiatives/initiativesnumber"
        : "http://localhost:3001/initiatives/initiativesnumber",
      config
    );
    setInitiativesNumber(data.initiativesNumber);
  };

  /////////////////////////////////////////////////////////////////////////
  const navigateToFormStep = (stepNumber) => {
    /**
     * Hide all form steps.
     */
    document.querySelectorAll(".form-step").forEach((formStepElement) => {
      formStepElement.classList.add("d-none");
    });
    /**
     * Mark all form steps as unfinished.
     */
    document
      .querySelectorAll(".form-stepper-list")
      .forEach((formStepHeader) => {
        formStepHeader.classList.add("form-stepper-unfinished");
        formStepHeader.classList.remove(
          "form-stepper-active",
          "form-stepper-completed"
        );
      });
    /**
     * Show the current form step (as passed to the function).
     */
    document.querySelector("#step-" + stepNumber).classList.remove("d-none");
    /**
     * Select the form step circle (progress bar).
     */
    const formStepCircle = document.querySelector(
      'li[step="' + stepNumber + '"]'
    );
    /**
     * Mark the current form step as active.
     */
    formStepCircle.classList.remove(
      "form-stepper-unfinished",
      "form-stepper-completed"
    );
    formStepCircle.classList.add("form-stepper-active");
    /**
     * Loop through each form step circles.
     * This loop will continue up to the current step number.
     * Example: If the current step is 3,
     * then the loop will perform operations for step 1 and 2.
     */
    for (let index = 0; index < stepNumber; index++) {
      /**
       * Select the form step circle (progress bar).
       */
      const formStepCircle = document.querySelector('li[step="' + index + '"]');
      /**
       * Check if the element exist. If yes, then proceed.
       */
      if (formStepCircle) {
        /**
         * Mark the form step as completed.
         */
        formStepCircle.classList.remove(
          "form-stepper-unfinished",
          "form-stepper-active"
        );
        formStepCircle.classList.add("form-stepper-completed");
      }
    }
  };
  /**
   * Select all form navigation buttons, and loop through them.
   */
  document
    .querySelectorAll(".btn-navigate-form-step")
    .forEach((formNavigationBtn) => {
      /**
       * Add a click event listener to the button.
       */
      formNavigationBtn.addEventListener("click", () => {
        /**
         * Get the value of the step.
         */
        const stepNumber = parseInt(
          formNavigationBtn.getAttribute("step_number")
        );
        /**
         * Call the function to navigate to the target form step.
         */
        navigateToFormStep(stepNumber);
      });
    });
  /////////////////////////////////////////////////////////////////////////
  document
    .querySelectorAll(".form-stepper-circle")
    .forEach((formNavigationBtn) => {
      /**
       * Add a click event listener to the button.
       */
      formNavigationBtn.addEventListener("click", () => {
        /**
         * Get the value of the step.
         */
        const stepNumber = parseInt(
          formNavigationBtn.getAttribute("step_number")
        );
        /**
         * Call the function to navigate to the target form step.
         */
        navigateToFormStep(stepNumber);
      });
    });
  /////////////////////////////////////////////////////////////////////////

  useEffect(() => {
    document.title = "Working Area";

    // Getting the User from the
    setProfile(JSON.parse(localStorage.getItem("profile")));
    if (!profile?.token) {
      window.location = "/login";
    }
    getUnitsNumber();
    getScansNumber();
    getParticipantsNumber();
    getTotalParticipantsNumber();
    getTotalScansNumber();
    getTotalUnitsNumber();
    getTotalUniversitiesNumber();
    getInitiativesNumber();
    //////////////////////////////////////////
  }, [location]);

  return (
    <>
      <div>
        <ul class="form-stepper form-stepper-horizontal text-center mx-auto pl-0">
          {/* Step 1*/}
          <li
            class="form-stepper-active text-center form-stepper-list"
            step="1"
          >
            <a class="mx-2">
              <span
                class="form-stepper-circle"
                style={{ cursor: "pointer" }}
                step_number="1"
              >
                <span>1</span>
              </span>
              <button
                class="button btn-navigate-form-step"
                type="button"
                step_number="1"
              >
                {t("Working_Area_Create_Units_Button_1")}
              </button>
            </a>
          </li>
          {/*  Step 2 */}
          <li
            class="form-stepper-unfinished text-center form-stepper-list"
            step="2"
          >
            <a class="mx-2">
              <span
                class="form-stepper-circle text-muted"
                style={{ cursor: "pointer" }}
                step_number="2"
              >
                <span>2</span>
              </span>
              <button
                class="button btn-navigate-form-step"
                type="button"
                step_number="2"
              >
                {t("Working_Area_Collect_Data_Button_1")}
              </button>
            </a>
          </li>
          {/* Step 3  */}
          <li
            class="form-stepper-unfinished text-center form-stepper-list"
            step="3"
          >
            <a class="mx-2">
              <span
                class="form-stepper-circle text-muted"
                style={{ cursor: "pointer" }}
                step_number="3"
              >
                <span>3</span>
              </span>
              <button
                class="button btn-navigate-form-step"
                type="button"
                step_number="3"
              >
                {" "}
                {t("Working_Area_Monitor_Units_Button_1")}
              </button>
            </a>
          </li>
        </ul>
        <form
          id="userAccountSetupForm"
          name="userAccountSetupForm"
          enctype="multipart/form-data"
          method="POST"
        >
          <section id="step-1" class="form-step first_step">
            <div class="mt-3">
              <p>{t("Working_Area_Create_Units_Text_1")}</p>
            </div>
            <div class="mt-3">
              <Link to="/createunit">
                <tr class="centred-button">
                  <input
                    type="Button"
                    value={t("Working_Area_Create_Units_Button_2")}
                    class="button"
                  />
                </tr>
                {/* <button className="centred-button" type="button" step_number="1"> Create your units </button> */}
              </Link>
              <Link to="/manageunit">
                <tr class="centred-button">
                  <input
                    type="Button"
                    value={t("Working_Area_Create_Units_Button_3")}
                    class="button"
                  />
                </tr>
                {/* <button className="centred-button" type="button" step_number="1"> Manage your units </button> */}
              </Link>
              <Link to="/trackunits">
                <tr class="centred-button">
                  <input
                    type="Button"
                    value={t("Working_Area_Create_Units_Button_4")}
                    class="button"
                  />
                </tr>
                {/* <button className="centred-button" type="button" step_number="1"> Track your participants</button> */}
              </Link>
              <tr class="centred-button">
                <button
                  className="button btn-navigate-form-step"
                  type="button"
                  step_number="2"
                  style={{ width: "60px" }}
                >
                  <img
                    width="40px"
                    height="40px"
                    src="images/east_white.png"
                    alt=""
                  />
                </button>
              </tr>
            </div>
            <div class="mt-3">
              <h5>
                {t("Working_Area_Create_Units_Text_2")}: {unitsNumber}
              </h5>
              <h5>
                {t("Working_Area_Create_Units_Text_3")}: {participantsNumber}
              </h5>
            </div>
            {/* <div className="image">
                <img 
                  width="400px"
                  height="430px"
                  src="images/step_1.jpg"
                  alt=""
                /> 
              </div> */}
          </section>
          <section id="step-2" class="form-step d-none second_step">
            <div class="mt-3">
              <p>{t("Working_Area_Collect_Data_Text_1")}</p>
            </div>
            <div class="mt-3">
              <Link to="/scansinformation">
                <tr class="centred-button">
                  <input
                    type="Button"
                    value={t("Working_Area_Collect_Data_Button_2")}
                    class="button"
                  />
                </tr>
              </Link>
              <Link to="/scangenerator">
                <tr class="centred-button">
                  <input
                    type="Button"
                    value={t("Working_Area_Collect_Data_Button_3")}
                    class="button"
                  />
                </tr>
              </Link>
              <Link to="/tracklinks">
                <tr class="centred-button">
                  <input
                    type="Button"
                    value={t("Working_Area_Collect_Data_Button_4")}
                    class="button"
                  />
                </tr>
              </Link>
              <tr class="centred-button">
                <button
                  className="button btn-navigate-form-step"
                  type="button"
                  step_number="1"
                  style={{ width: "60px", margin: "5px" }}
                >
                  <img
                    width="40px"
                    height="40px"
                    src="images/west_white.png"
                    alt=""
                  />{" "}
                </button>
                <button
                  className="button btn-navigate-form-step"
                  type="button"
                  step_number="3"
                  style={{ width: "60px", margin: "5px" }}
                >
                  <img
                    width="40px"
                    height="40px"
                    src="images/east_white.png"
                    alt=""
                  />{" "}
                </button>
              </tr>
            </div>
            <div class="mt-3">
              <h5>
                {t("Working_Area_Collect_Data_Text_2")}: {scansNumber}
              </h5>
              <h5>
                {" "}
                {t("Working_Area_Collect_Data_Text_3")}: {scansNumber}
              </h5>
            </div>
            {/* <div className="image" >
                <img 
                  src="images/step_2.jpg"
                  width="400px"
                  height="478px"
                  alt=""
                /> 
              </div> */}
          </section>
          <section id="step-3" class="form-step d-none third_step">
            <div class="mt-3">
              <p>{t("Working_Area_Monitor_Units_Text_1")}</p>
            </div>
            <div class="mt-3">
              <Link to="/dashboards/unitanalytics">
                <tr class="centred-button">
                  <input
                    type="Button"
                    value={t("Working_Area_Monitor_Units_Button_2")}
                    class="button"
                  />
                </tr>
              </Link>
              <tr class="centred-button">
                <button
                  className="button btn-navigate-form-step"
                  type="button"
                  step_number="2"
                  style={{ width: "60px" }}
                >
                  <img
                    width="40px"
                    height="40px"
                    src="images/west_white.png"
                    alt=""
                  />{" "}
                </button>
              </tr>
            </div>
            <div class="mt-3">
              <h5>
                {t("Working_Area_Monitor_Units_Text_2")}: {totalUnits}
              </h5>
              <h5>
                {t("Working_Area_Monitor_Units_Text_3")}: {totalParticipants}
              </h5>
              <h5>
                {t("Working_Area_Monitor_Units_Text_4")}: {totalUniversities}
              </h5>
              <h5>
                {t("Working_Area_Monitor_Units_Text_5")}: {totalScans}
              </h5>
            </div>
            {/* <div className="image">
                <img 
                  src="images/step_3.jpg"
                  width="400px"
                  height="446px"
                  alt=""
                /> 
              </div> */}
          </section>
        </form>
      </div>
      {/* <Footer /> */}
    </>
  );
}

export default WorkingArea;
