import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import Footer from "../../components/Footer";
import "./Login.css";
import axios from "axios";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function ManageUnit() {
  const [unit, setUnit] = useState({
    unitId: "",
    unitName: "",
    unitType: "",
    unitDescription: "",
    accessType: "",
  });

  const [units, setUnits] = useState([]);
  const [text, setText] = useState("");
  const [toggleConfirmation, setToggleConfirmation] = useState(false);

  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );
  const location = useLocation();

  const { t } = useTranslation();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${profile.token}`,
    },
  };

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUnit({ ...unit, [fieldName]: fieldValue });
  };

  const handleUpdate = async (e) => {
    e.preventDefault();
    if (!(unit.unitId === "")) {
      const { data } = await axios.put(
        process.env.REACT_APP_ENV === "production"
          ? `/api/units/manageunit/update/${unit.unitId}`
          : `http://localhost:3001/units/manageunit/update/${unit.unitId}`,
        {
          unit: unit,
        },
        config
      );
      setText(data.message);
    } else {
      setText("Manage_Unit_Validation_3");
    }
    window.location = "/manageunit";
  };

  const handleConfirmation = (e) => {
    e.preventDefault();
    if (!(unit.unitId === "")) {
      if (!toggleConfirmation) {
        setToggleConfirmation(true);
        setText("Manage_Unit_Validation_4");
      } else {
        setToggleConfirmation(false);
        setText("");
      }
    } else {
      setText("Manage_Unit_Validation_3");
    }
  };

  const handleDelete = async (e) => {
    e.preventDefault();
    console.log(`deleting unit ${unit.unitId}`);
    const { data } = await axios.delete(
      process.env.REACT_APP_ENV === "production"
        ? `/api/units/manageunit/delete/${unit.unitId}`
        : `http://localhost:3001/units/manageunit/delete/${unit.unitId}`,
      config,
      {
        unitId: unit.unitId,
      }
    );

    setText(data.message);
    window.location = "/manageunit";
  };

  const fetchData = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/units/manageunit"
        : "http://localhost:3001/units/manageunit",
      config
    );
    setUnits(data.unitsResult);
  };

  const fetchCurrentUnit = async () => {
    console.log("Entering fetch Current");
    units.map((element) => {
      if (`${element.idunit}` === unit.unitId) {
        setUnit({
          unitId: unit.unitId,
          unitName: element.name,
          unitType: element.type,
          unitDescription: element.description,
          accessType: element.access,
        });

        console.log(element);
      }
    });
    console.log(unit);
  };

  useEffect(() => {
    document.title = "Manage your units";
    setProfile(JSON.parse(localStorage.getItem("profile")));
    if (!profile?.token) {
      window.location = "/login";
    }
    fetchData();
    fetchCurrentUnit();
    console.log(units);
  }, [location, unit.unitId]);

  return (
    <>
      <div className="box">
        <img
          className="sideimg"
          width="50%"
          height="1400px"
          src="images/photo4.jpg"
          alt=""
        />
        <article className="form">
          <form>
            <div className="title">
              <p> {t("Manage_Unit_Title")}</p>
            </div>
            <div>
              <label htmlFor="unit"> {t("Manage_Unit_Change")}: </label>
              <select
                id="unitId"
                name="unitId"
                value={unit.unitId}
                onChange={handleChange}
              >
                <option>{t("Select_Unit")} </option>
                {units?.map((unit) => (
                  <option key={unit.idunit} value={unit.idunit}>
                    {unit.name}
                  </option>
                ))}
              </select>
            </div>

            <div>
              <label htmlFor="unitName"> {t("Manage_Unit_Name")}:</label>
              <input
                type="text"
                id="unitName"
                name="unitName"
                value={unit.unitName}
                onChange={handleChange}
              />
            </div>
            <div>
              <label htmlFor="unitType"> {t("Manage_Unit_Type")}: </label>
              <select
                id="unitType"
                name="unitType"
                value={unit.unitType}
                onChange={handleChange}
              >
                <option> {t("Select_Type")} </option>
                <option value="Computer Science"> Computer Science</option>
                <option value="Mechanics"> Mechanics</option>
                <option value="Electronics"> Electronics</option>
                <option value="Aerodynamics"> Aerodynamics</option>
                <option value="Civil Engineering"> Civil engineering</option>
                <option value="Library"> Library</option>
                <option value="Biology"> Biology</option>
                <option value="Medecine"> Medecine</option>
                <option value="Litterature"> Economics</option>
                <option value="Management"> Management</option>
                <option value="Mathematics"> Mathematics</option>
                <option value="Physics"> Physics</option>
                <option value="Law"> Law</option>
                <option value="History"> History</option>
                <option value="Arts"> Arts</option>
              </select>
            </div>
            <div>
              <label>{t("Manage_Unit_Other_Type")}:</label>
              <input
                type="text"
                id="unitType"
                name="unitType"
                value={unit.unitType}
                onChange={handleChange}
              />
            </div>

            <label htmlFor="unitDescription">
              {" "}
              {t("Manage_Unit_Description")}:
            </label>
            <div>
              <textarea
                name="unitDescription"
                id="unitDescription"
                value={unit.unitDescription}
                onChange={handleChange}
                cols="50"
                rows="5"
              ></textarea>
            </div>

            <div>
              <label htmlFor="accessType">{t("Manage_Unit_Access")}: </label>
              <select
                id="accessType"
                name="accessType"
                value={unit.accessType}
                onChange={handleChange}
              >
                <option value="Public">{t("Create_Unit_Public")}</option>
                <option value="Private">{t("Create_Unit_Private")}</option>
              </select>
            </div>

            <div>
              <tr class="centred-btn">
                <button type="submit" className="btn" onClick={handleUpdate}>
                  {t("Manage_Unit_Button_2")}
                </button>
              </tr>
              <Link to="/workingarea">
                <tr class="centred-btn">
                  <button className="btn">{t("Manage_Unit_Button_3")}</button>
                </tr>
              </Link>
              {/* <tr class="centred-btn">
                <button className="btn" onClick={handleConfirmation}>
                  {t("Manage_Unit_Button_1")}
                </button>
              </tr> */}

              <div>
                <tr class="centred-btn">
                  <button className="btn" onClick={handleConfirmation}>
                    {t("Manage_Unit_Button_1")}
                  </button>
                </tr>
              </div>
            </div>
            <tr class="centred-btn">
              <h5 style={{ color: "red", marginTop: "2rem" }}>{t(text)}</h5>
            </tr>

            {toggleConfirmation && (
              <>
                <div>
                  <tr class="centred-btn">
                    <button
                      style={{ backgroundColor: "red" }}
                      className="btn"
                      onClick={handleDelete}
                    >
                      {t("Continue")}
                    </button>
                  </tr>
                  <tr class="centred-btn">
                    <button
                      style={{ backgroundColor: "orange" }}
                      className="btn"
                      onClick={handleConfirmation}
                    >
                      {t("Abondon")}
                    </button>
                  </tr>
                </div>
              </>
            )}
          </form>

          {/* {toggleConfirmation && (
            <>
              <div>
                <tr class="centred-btn">
                  <button
                    style={{ backgroundColor: "red" }}
                    className="btn"
                    onClick={handleDelete}
                  >
                    Continue !
                  </button>
                </tr>
                <tr class="centred-btn">
                  <button
                    style={{ backgroundColor: "orange" }}
                    className="btn"
                    onClick={handleConfirmation}
                  >
                    Abondon Deleting
                  </button>
                </tr>
              </div>
            </>
          )} */}
        </article>
      </div>
      {/* <Footer /> */}
    </>
  );
}

export default ManageUnit;
