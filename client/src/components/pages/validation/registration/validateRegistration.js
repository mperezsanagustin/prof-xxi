export default function validateRegistration(user) {
  let errors = {};

  if (user.firstName === "" || user.lastName === "") {
    errors.userName = "Registration_Validation_1";
  }

  if (user.organizationName === "") {
    errors.organizationName = "Registration_Validation_2";
  }

  if (user.email === "") {
    errors.email = "Registration_Validation_3";
  } else if (!/\S+@\S+\.\S+/.test(user.email)) {
    errors.email = "Registration_Validation_4";
  }

  if (user.password === "") {
    errors.password = "Registration_Validation_5";
  }
  return errors;
}
