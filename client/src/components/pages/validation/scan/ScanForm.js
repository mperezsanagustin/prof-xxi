import { useState, useEffect } from "react";
import validateScan from "./validateScan";

const ScanForm = (Submit) => {
  const [userAnswers, setUsersAnswers] = useState({
    A1: "1",
    A2: "1",
    A3: "1",
    A4: "1",
    A5: "1",
    A6: "1",
    A7: "1",
    A8: "1",
    A9: "1",
    A10: "1",
    A11: "1",
    B1: "1",
    B2: "1",
    B3: "1",
    B4: "1",
    B5: "1",
    B6: "1",
    B7: "1",
    B8: "1",
    B9: "1",
    B10: "1",
    B11: "1",
    C1: "1",
    C2: "1",
    C3: "1",
    C4: "1",
    C5: "1",
    C6: "1",
    C7: "1",
    C8: "1",
    C9: "1",
    C10: "1",
    C11: "1",
    D1: "1",
    D2: "1",
    D3: "1",
    D4: "1",
    D5: "1",
    D6: "1",
    D7: "1",
    D8: "1",
    D9: "1",
    E1: "1",
    E2: "1",
    E3: "1",
    E4: "1",
    E5: "1",
    E6: "1",
    E7: "1",
    E8: "1",
  });

  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    position: "",
  });

  const [isSubmitting, setIsSubmitting] = useState(false);

  const [errors, setErrors] = useState({});

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUsersAnswers({ ...userAnswers, [fieldName]: fieldValue });
  };

  const handleUserChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUser({ ...user, [fieldName]: fieldValue });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setErrors(validateScan(user, userAnswers));
    setIsSubmitting(true);
  };

  useEffect(() => {
    console.log(errors);
    console.log(isSubmitting);
    if (Object.keys(errors).length === 0 && isSubmitting) {
      Submit();
    }
  }, [errors]);

  return {
    handleChange,
    handleUserChange,
    handleSubmit,
    user,
    userAnswers,
    errors,
  };
};

export default ScanForm;
