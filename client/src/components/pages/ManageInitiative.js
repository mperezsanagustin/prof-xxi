import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import "./Login.css";
import "./Initiatives.css";
import axios from "axios";
import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";

function ManageInitiative() {
  const [initiative, setInitiative] = useState({
    initiativeId: "",
    initiativeName: "",
    initiativeDescription: "",
    initiativeTechnologies: "",
    initiativeTime: "",
    accessType: "Public",
    dimA: false,
    dimB: false,
    dimC: false,
    dimD: false,
    dimE: false,
  });

  const [initiatives, setInitiatives] = useState([]);
  const [text, setText] = useState("");
  const [toggleConfirmation, setToggleConfirmation] = useState(false);

  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );
  const location = useLocation();

  const { t } = useTranslation();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${profile.token}`,
    },
  };

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setInitiative({ ...initiative, [fieldName]: fieldValue });
  };

  const handleCheckBox = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.checked;

    setInitiative({ ...initiative, [fieldName]: fieldValue });
  };

  useEffect(() => {
    console.log(initiative);
  }, [initiative]);

  const handleUpdate = async (e) => {
    e.preventDefault();
    if (!(initiative.initiativeId === "")) {
      const { data } = await axios.put(
        process.env.REACT_APP_ENV === "production"
          ? `/api/initiatives/manageinitiative/update/${initiative.initiativeId}`
          : `http://localhost:3001/initiatives/manageinitiative/update/${initiative.initiativeId}`,
        {
          initiative: initiative,
        },
        config
      );
      setText(data.message);
    } else {
      setText("Please select a initiative");
    }
    window.location = "/manageinitiative";
  };

  const fetchData = async (e) => {
    const { data } = await axios.get(
      process.env.REACT_APP_ENV === "production"
        ? "/api/initiatives/manageinitiative"
        : "http://localhost:3001/initiatives/manageinitiative",
      config
    );
    setInitiatives(data.initiativesResult);
  };

  const fetchCurrentInitiative = async () => {
    console.log("Entering fetch Current");
    initiatives.map((element) => {
      if (`${element.idinitiative}` === initiative.initiativeId) {
        setInitiative({
          initiativeId: initiative.initiativeId,
          initiativeName: element.name,
          initiativeDescription: element.description,
          initiativeTechnologies: element.technologies,
          initiativeTime: element.time,
          accessType: element.access,
          dimA: element.dimA === "1" ? true : false,
          dimB: element.dimB === "1" ? true : false,
          dimC: element.dimC === "1" ? true : false,
          dimD: element.dimD === "1" ? true : false,
          dimE: element.dimE === "1" ? true : false,
        });

        console.log(element);
      }
    });
    console.log(initiative);
  };

  useEffect(() => {
    document.title = "Manage your initiatives";
    setProfile(JSON.parse(localStorage.getItem("profile")));
    if (!profile?.token) {
      window.location = "/login";
    }
    fetchData();
    fetchCurrentInitiative();
    console.log(initiatives);
  }, [location, initiative.initiativeId]);

  return (
    <>
      <div className="box">
        <img
          className="sideimg"
          width="50%"
          height="1200px"
          src="images/photo7.jpg"
          alt=""
        />
        <article className="form">
          <form>
            <div className="title">
              <p>{t("Manage_Initiative_Title1")} </p>
            </div>
            <div>
              <label htmlFor="initiative">
                {" "}
                {t("Manage_Initiative_Title2")}:{" "}
              </label>
              <select
                id="initiativeId"
                name="initiativeId"
                value={initiative.initiativeId}
                onChange={handleChange}
              >
                <option>{t("Manage_Initiative_Text")} </option>
                {initiatives?.map((initiative) => (
                  <option
                    key={initiative.idinitiative}
                    value={initiative.idinitiative}
                  >
                    {initiative.name}
                  </option>
                ))}
              </select>
            </div>

            <div>
              <label htmlFor="initiativeName">
                {t("Create_Initiative_Title_2")}:
              </label>
              <input
                type="text"
                id="initiativeName"
                name="initiativeName"
                value={initiative.name}
                onChange={handleChange}
              />
            </div>

            <label htmlFor="initiativeDescription">
              {" "}
              {t("Create_Initiative_Title_3")}:
            </label>
            <div>
              <textarea
                name="initiativeDescription"
                id="initiativeDescription"
                value={initiative.initiativeDescription}
                onChange={handleChange}
                cols="50"
                rows="5"
              ></textarea>
            </div>

            <div>
              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimA"
                  name="dimA"
                  value="dimA"
                  checked={initiative.dimA}
                  onChange={handleCheckBox}
                />
                <span
                  className="check_text_dim"
                  style={{ color: "#224E9F", fontSize: "large" }}
                >
                  {t("Create_Initiative_A")}{" "}
                </span>
              </div>

              <label htmlFor="dimB"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimB"
                  name="dimB"
                  value="dimB"
                  checked={initiative.dimB}
                  onChange={handleCheckBox}
                />
                <span
                  className="check_text_dim"
                  style={{ color: "#224E9F", fontSize: "large" }}
                >
                  {t("Create_Initiative_B")}
                </span>
              </div>

              <label htmlFor="dimC"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimC"
                  name="dimC"
                  value="dimC"
                  checked={initiative.dimC}
                  onChange={handleCheckBox}
                />
                <span
                  className="check_text_dim"
                  style={{ color: "#224E9F", fontSize: "large" }}
                >
                  {t("Create_Initiative_C")}
                </span>
              </div>

              <label htmlFor="dimD"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimD"
                  name="dimD"
                  value="dimD"
                  checked={initiative.dimD}
                  onChange={handleCheckBox}
                />
                <span
                  className="check_text_dim"
                  style={{ color: "#224E9F", fontSize: "large" }}
                >
                  {t("Create_Initiative_D")}
                </span>
              </div>

              <label htmlFor="dimE"></label>

              <div className="check_dim">
                <input
                  type="checkbox"
                  id="dimE"
                  name="dimE"
                  value="dimE"
                  checked={initiative.dimE}
                  onChange={handleCheckBox}
                />
                <span
                  className="check_text_dim"
                  style={{ color: "#224E9F", fontSize: "large" }}
                >
                  {t("Create_Initiative_E")}
                </span>
              </div>
            </div>

            <label htmlFor="initiativeTechnologies">
              {" "}
              {t("Create_Initiative_Title_4")}:
            </label>
            <div>
              <input
                type="text"
                id="initiativeTechnologies"
                name="initiativeTechnologies"
                value={initiative.initiativeTechnologies}
                onChange={handleChange}
                placeholder="Google Meet, Zoom..."
              />
            </div>

            <div>
              <label htmlFor="accessType">
                {" "}
                {t("Create_Initiative_Title_5")}:
              </label>
              <select
                id="accessType"
                name="accessType"
                value={initiative.accessType}
                onChange={handleChange}
              >
                <option value="Public">{t("Create_Unit_Public")}</option>
                <option value="Private">{t("Create_Unit_Private")}</option>
              </select>
            </div>

            <div>
              <tr class="centred-btn">
                <button type="submit" className="btn" onClick={handleUpdate}>
                  {t("Manage_Initiative_Button_1")}
                </button>
              </tr>
              <Link to="/workingarea">
                <tr class="centred-btn">
                  <button className="btn">
                    {" "}
                    {t("Manage_Initiative_Button_2")}
                  </button>
                </tr>
              </Link>
            </div>
            <tr class="centred-btn">
              <h5 style={{ color: "red", marginTop: "2rem" }}>{t(text)}</h5>
            </tr>
          </form>
        </article>
      </div>
    </>
  );
}

export default ManageInitiative;
