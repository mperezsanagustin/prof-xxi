import Axios from "axios";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./Login.css";

import i18n from "./../../i18n";
import { useTranslation } from "react-i18next";
import ReactTooltip from "react-tooltip";

function CreateUnit() {
  const [unit, setUnit] = useState({
    unitName: "",
    unitType: "",
    unitDescription: "",
    accessType: "Public",
  });

  const [text, setText] = useState("");
  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );
  const { t } = useTranslation();

  const handleChange = (e) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;
    setUnit({ ...unit, [fieldName]: fieldValue });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (profile) {
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${profile.token}`,
        },
      };

      if (unit.unitName === "") {
        setText("Create_Unit_Validation_4");
      } else {
        Axios.post(
          process.env.REACT_APP_ENV === "production"
            ? "/api/units/createunit"
            : "http://localhost:3001/units/createunit",
          { unit: unit },
          config
        ).then((response) => {
          if (response.data.created) {
            setText("Create_Unit_Validation_1");
          } else if (response.data?.status === "existing") {
            setText("Create_Unit_Validation_3");

            setText(response.data.message);
          } else {
            setText("Create_Unit_Validation_2");
          }
        });
      }
    }
  };

  useEffect(() => {
    document.title = "Create your unit";
    console.log(profile);
    if (!profile?.token) {
      window.location = "/login";
    }
  }, []);

  return (
    <>
      <div className="box">
        <img
          className="sideimg"
          width="50%"
          height="1000px"
          src="images/photo3.jpg"
          alt=""
        />
        <article className="form">
          <form>
            <div className="title">
              <p>{t("Create_Unit_Title")}</p>
            </div>
            <div>
              <label htmlFor="unitName"> {t("Create_Unit_Name")}:</label>
              <input
                type="text"
                id="unitName"
                name="unitName"
                value={unit.name}
                onChange={handleChange}
              />
            </div>
            <div>
              <label htmlFor="unitType"> {t("Create_Unit_Type")}: </label>
              <select
                id="unitType"
                name="unitType"
                value={unit.unitType}
                onChange={handleChange}
              >
                <option> {t("Select_Type")} </option>
                <option value="Computer Science"> Computer Science</option>
                <option value="Mechanics"> Mechanics</option>
                <option value="Electronics"> Electronics</option>
                <option value="Aerodynamics"> Aerodynamics</option>
                <option value="Civil Engineering"> Civil engineering</option>
                <option value="Library"> Library</option>
                <option value="Biology"> Biology</option>
                <option value="Medecine"> Medecine</option>
                <option value="Litterature"> Economics</option>
                <option value="Management"> Management</option>
                <option value="Mathematics"> Mathematics</option>
                <option value="Physics"> Physics</option>
                <option value="Law"> Law</option>
                <option value="History"> History</option>
                <option value="Arts"> Arts</option>
              </select>
            </div>
            <div>
              <label> {t("Create_Unit_Other_Type")}:</label>
              <input
                type="text"
                id="unitType"
                name="unitType"
                value={unit.type}
                onChange={handleChange}
              />
            </div>
            <div>
              <label htmlFor="unitDescription">
                {" "}
                {t("Create_Unit_Description")}:
              </label>

              <div>
                <textarea
                  name="unitDescription"
                  id="unitDescription"
                  value={unit.unitDescription}
                  onChange={handleChange}
                  cols="50"
                  rows="5"
                ></textarea>
              </div>
            </div>

            <div>
              <label htmlFor="accessType"> {t("Create_Unit_Access")}:</label>
              <img
                className="information"
                data-tip
                data-for="unit1Tip"
                src="/images/info_icon.png"
                width="20px"
                height="20px"
                alt=""
              />
              <ReactTooltip
                id="unit1Tip"
                place="bottom"
                effect="solid"
                className="tooltip"
              >
                {t("Create_Unit_Access_info")}
              </ReactTooltip>
              <select
                id="accessType"
                name="accessType"
                value={unit.accessType}
                onChange={handleChange}
              >
                <option value="Public">{t("Create_Unit_Public")}</option>
                <option value="Private">{t("Create_Unit_Private")}</option>
              </select>
            </div>

            <div>
              <tr class="centred-btn">
                <button type="submit" className="btn" onClick={handleSubmit}>
                  {t("Create_Unit_Button_1")}
                </button>
              </tr>
              <Link to="/workingarea">
                <tr class="centred-btn">
                  <button className="btn"> {t("Create_Unit_Button_2")} </button>
                </tr>
              </Link>
            </div>
            <tr class="centred-btn">
              <h5 style={{ color: "red", marginTop: "2rem" }}>{t(text)}</h5>
            </tr>
          </form>
        </article>
      </div>
      {/* <Footer /> */}
    </>
  );
}

export default CreateUnit;
