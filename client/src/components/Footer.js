import React from "react";
import "./Footer.css";
import { Link } from "react-router-dom";

//<div class="footer-link items">
// <h2>PROF XXI © 2021</h2>
//  <h3> Under construction</h3>
// </div>;

function Footer() {
  return (
    <>
      <div className="footer-container">
        <div className="footer-links">
          <a className="logo" href="https://www.uc3m.es/home">
            <img
              width="110"
              height="110"
              style={{ marginTop: -15 }}
              src="/images/uc3m_trans.png"
              alt=""
            />
          </a>

          <a className="logo" href="https://portal.uab.pt/en/">
            <img
              width="80"
              height="50"
              style={{ marginTop: 20 }}
              src="/images/aberta_trans.png"
              alt=""
            />
          </a>

          <a className="logo" href="https://www.univ-tlse3.fr/">
            <img
              width="100"
              height="60"
              style={{ margin: 10 }}
              src="/images/ut3_trans.png"
              alt=""
            />
          </a>

          <a className="logo" href="https://www.galileo.edu/">
            <img
              width="100"
              height="50"
              style={{ margin: 10 }}
              src="/images/galileo_trans.png"
              alt=""
            />
          </a>

          <a
            className="logo"
            href="https://universidades.gt/universidades/universidad-san-carlos-de-guatemala"
          >
            <img
              width="90"
              height="40"
              style={{ marginTop: 15 }}
              src="/images/usac_trans.png"
              alt=""
            />
          </a>

          <a className="logo" href="http://www.unicauca.edu.co/versionP/">
            <img
              width="100"
              height="50"
              style={{ margin: 10 }}
              src="/images/cauca_trans.png"
              alt=""
            />
          </a>

          <a className="logo" href="https://www.usbcali.edu.co/">
            <img
              width="100"
              height="50"
              style={{ margin: 10 }}
              src="/images/cali_trans.png"
              alt=""
            />
          </a>

          <div className="footer-link-wrapper"></div>
        </div>
        <h4 className="signature">PROF XXI © 2021</h4>
      </div>
    </>
  );
}

export default Footer;
