import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import "./Navbar.css";
import decode from "jwt-decode";
import { LangContext } from "./../components/contexts/LangContext";
import LightSpeed from "react-reveal/LightSpeed";
import Flip from "react-reveal/Flip";

import i18n from "./../i18n";

// Translation
import { useTranslation } from "react-i18next";
import { useContext } from "react";

function Navbar() {
  const [button, setButton] = useState(true);
  const [profile, setProfile] = useState(
    JSON.parse(localStorage.getItem("profile"))
  );

  const location = useLocation();

  const { lang, setLang } = useContext(LangContext);

  const handleLogOut = () => {
    localStorage.removeItem("profile");
    window.location = "/";
  };

  // Translation
  const { t } = useTranslation();

  const handleTranslate = (lang) => {
    i18n.changeLanguage(lang);
    setLang(lang);
  };

  //

  useEffect(() => {
    const token = profile?.token;

    if (token) {
      const decodedToken = decode(token);

      setTimeout(() => {
        handleLogOut();
      }, 10000 * 60 * 60);

      if (decodedToken.exp * 1000 < new Date().getTime()) {
        handleLogOut();
      }
    }

    JSON.parse(localStorage.getItem("profile"));
  }, [location]);

  return (
    <>
      <nav className="navbar">
        <div className="navbar-container">
          <a href="http://www.profxxi.org/?lang=en" className="navbar-logo">
            <LightSpeed left>
              <img
                src="/images/profxxi_eu.png"
                alt=""
                width="50px"
                height="50px"
              />
            </LightSpeed>
          </a>
          <a href="https://www.irit.fr/en/home/">
            <LightSpeed left>
              <img src="/images/irit.png" alt="" className="navbar-irit" />
            </LightSpeed>
          </a>

          <ul className="navbar-menu">
            <li className="navbar-item">
              <Link to="/" className="navbar-links">
                {t("Navbar_Home")}
              </Link>
            </li>

            <li className="navbar-item">
              <Link to="/about" className="navbar-links">
                {t("Navbar_About_Us")}
              </Link>
            </li>

            {!profile && (
              <li className="navbar-item">
                <Link to="/register" className="navbar-links">
                  {t("Navbar_Register")}
                </Link>
              </li>
            )}

            <li className="navbar-item">
              <Link to="/initiatives" className="navbar-links">
                {t("Navbar_Initiatives")}
              </Link>
            </li>

            {!profile && (
              <li className="navbar-item">
                <Link to="/login" className="navbar-links">
                  {t("Navbar_Login")}
                </Link>
              </li>
            )}

            {profile && (
              <>
                <li className="navbar-item navigation">
                  {/* <div class="navigation"> */}
                  <Link to="/workingarea" className="navbar-links">
                    {t("Navbar_Working_Area")}
                  </Link>
                  <div class="navigation-content">
                    <a href="/createunit" style={{ paddingTop: "10px" }}>
                      {t("Navbar_Working_Area_1")}
                    </a>
                    <a href="/scansinformation" style={{ paddingTop: "10px" }}>
                      {t("Navbar_Working_Area_2")}
                    </a>
                    <a
                      href="/dashboards/unitanalytics"
                      style={{ paddingTop: "10px", paddingBottom: "10px" }}
                    >
                      {t("Navbar_Working_Area_3")}
                    </a>
                  </div>
                  {/* </div> */}
                </li>

                <li className="navbar-item">
                  <button className="navbar-button" onClick={handleLogOut}>
                    {t("Navbar_Log_Out")}
                  </button>
                </li>
              </>
            )}
          </ul>
          <div className="navbar-lang">
            <button
              className="navbar-lang-item"
              onClick={() => handleTranslate("sp")}
            >
              <Flip left>
                <img width="30" height="30" src="/images/sp.png"></img>
              </Flip>
            </button>

            <button
              className="navbar-lang-item"
              onClick={() => handleTranslate("en")}
            >
              <Flip left>
                <img width="30" height="30" src="/images/en.png"></img>
              </Flip>
            </button>

            <button
              className="navbar-lang-item"
              onClick={() => handleTranslate("fr")}
            >
              <Flip left>
                <img width="30" height="30" src="/images/fr.png"></img>
              </Flip>
            </button>

            {profile && (
              <>
                <Link to="/updateprofile">
                  <img
                    className="navbar-lang-user"
                    src="/images/user.png"
                    alt=""
                    width="40px"
                    height="40px"
                  />
                </Link>
              </>
            )}
          </div>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
