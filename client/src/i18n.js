import i18n from "i18next";
import Backend from "i18next-http-backend";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

const Languages = ["en", "sp", "fr"];

i18n
  .use(Backend)
  .use(initReactI18next) // bind react-i18next to the instance
  .init({
    fallbackLng: "en",
    keySeparator: false, // we do not use keys in form messages.welcome

    lng: "en",
    supportedLngs: ["en", "sp", "fr"],
    debug: true,
    whitelist: Languages,

    // react-i18next options
    react: {
      wait: true,
    },

    // react i18next special options (optional)
    // override if needed - omit if ok with defaults
    /*
    react: {
      bindI18n: 'languageChanged',
      bindI18nStore: '',
      transEmptyNodeValue: '',
      transSupportBasicHtmlNodes: true,
      transKeepBasicHtmlNodesFor: ['br', 'strong', 'i'],
      useSuspense: true,
    }
    */
  });

export default i18n;
